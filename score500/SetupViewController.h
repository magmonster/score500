//
//  SetupViewController.h
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

// Views
#import "GameViewController.h"

// Data
#import "Setup.h"
#import "Team.h"

// Utilities
#import "Colors.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "BSKeyboardControls.h"



// Banners
@class GADBannerView;
@import GoogleMobileAds;



@class SetupViewController;




@interface SetupViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate, GADBannerViewDelegate, NSFetchedResultsControllerDelegate> {
	
	BOOL gameInProgress;
	int intGameRounds;
	CGFloat animatedDistance;
}

-(void)bannerConfig;


@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;


@property (strong, nonatomic) IBOutlet UIView *roundsMessageView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
-(IBAction)segmentValueChanged:(id)sender;


@property (strong, nonatomic) NSString *gameType;
@property (strong, nonatomic) NSNumber *numRounds;

@property (strong, nonatomic) IBOutlet UIView *borderTeam1;
@property (strong, nonatomic) IBOutlet UIView *borderTeam2;







@property (strong, nonatomic) IBOutlet UIControl *contentView;







@property (strong, nonatomic) Team *team;
@property (strong, nonatomic) NSArray *teamData;

@property (strong, nonatomic) Setup *setup_info;
@property (strong, nonatomic) NSArray *setupData;

@property (strong, nonatomic) Team *team_info_A;
@property (strong, nonatomic) Team *team_info_B;


-(IBAction)saveSetup:(id)sender;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



@property (strong, nonatomic) IBOutlet UIButton *btnBeginGame;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayTo500;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayToRounds;


@property (strong, nonatomic) IBOutlet UITextField *txtGameRounds;
@property (strong, nonatomic) IBOutlet UITextField *txtTeamNameA;
@property (strong, nonatomic) IBOutlet UITextField *txtTeamNameB;



// passed in from the Game view
@property (strong, nonatomic) NSString *teamNameA;
@property (strong, nonatomic) NSString *teamNameB;


// used for interacting with the keyboard, and moving the view
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
-(IBAction)dismissKeyboard:(id)sender;
-(void)setupKeyboardControls;
-(void)scrollViewToTextField:(id)textField;


@end
