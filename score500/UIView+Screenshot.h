//
//  UIView+Screenshot.h
//  Score500
//
//  Created by Drew Bombard on 4/4/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Screenshot)

-(UIImage *)convertViewToImage;

@end
