//
//  BiddingViewController.h
//  Score500
//
//  Created by Drew Bombard on 12/30/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "AppStats.h"
#import "Colors.h"

@interface BiddingViewController : UIViewController

- (IBAction)dismissModal:(id)sender;

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIView *containerView;


@end
