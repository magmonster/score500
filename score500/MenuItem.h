//
//  MenuItem.h
//  score500
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *name; // name of menu item
@property (nonatomic, strong) NSString *imageFile; // image filename of menu item

@end
