//
//  Page2ViewController.m
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import "Page2ViewController.h"

@interface Page2ViewController ()

@end

@implementation Page2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)page1ButtonTapped:(UIBarButtonItem *)sender
{
    [self.rootViewController goToPreviousContentViewController];
}

- (void)page3ButtonTapped:(UIBarButtonItem *)sender
{
    [self.rootViewController goToNextContentViewController];
}

@end
