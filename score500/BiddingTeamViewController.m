//
//  BiddingTeamViewController.m
//  Score500
//
//  Created by Drew Bombard on 12/30/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import "BiddingTeamViewController.h"

@interface BiddingTeamViewController ()

@end

@implementation BiddingTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	

	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	// Show the Ad Banners if this is the Free/Lite version
	if (_appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];
	
	if ([CoreDataCheck checkEntity:@"Setup"] > 0) {
		[self fetchSetupResults];
		_setup_info = [_setupData objectAtIndex:0];
	}

	[self setupTeamNamesGameType];

	
	[self loadPickerData];
	
	[self setupBiddingData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
	_bannerView.delegate = nil;
}


-(void)customizeInterface {
	
	_btnSave.backgroundColor = [Colors get].medGreen;
	_btnSave.titleLabel.textColor = [UIColor whiteColor];
	
	_btnSave.layer.cornerRadius =  _btnSave.frame.size.height/2;;
	_btnSave.layer.masksToBounds = YES;
}


-(void)fetchSetupResults {
	_teamData = [FetchDataArray dataFromEntity:@"Team" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	_setupData = [FetchDataArray dataFromEntity:@"Setup" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}


-(void)setupBiddingData {
	
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		[self fetchBiddingResults];
		_bidding_info = [_biddingData objectAtIndex:0];
		
		_bidTeam = _bidding_info.bidTeam;
		_biddingTeamName = _bidding_info.bidTeamName;
		
		
		if ([_bidTeam isEqual: @"teamA"]) {
			_segmentTeams.selectedSegmentIndex = 0;
		} else {
			_segmentTeams.selectedSegmentIndex = 1;
		}
		

		
		// Fetch bid indexes and reset picker
		[self resetPickerSelection:[_bidding_info.bidSuitIndex intValue]
						  bidIndex: [_bidding_info.bidNumIndex intValue]];
		
	} else {
		
		// Set default segment to Team 1
		_segmentTeams.selectedSegmentIndex = 0;
	}
}






-(IBAction)saveBid:(id)sender {
	
	NSLog(@"saveBid()");
	
	
	if (!_selectedTeamName) {
		[self segmentValueChanged:nil];
	}
	
	
	NSLog(@"selectedTeamName: %@",_selectedTeamName);
	
	
	NSString *selectedSuit = [_pickerSuitArray objectAtIndex:[_biddingPickerView selectedRowInComponent:0]];
	int bidSuitIndex = (int)[_biddingPickerView selectedRowInComponent:0];
	
	NSString *selectedBidCount = [_pickerBidArray objectAtIndex:[_biddingPickerView selectedRowInComponent:1]];
	int bidNumIndex = (int)[_biddingPickerView selectedRowInComponent:1];
	
	
	
// TRASH
//	if (selectedTeamName == [_pickerTeamArray objectAtIndex:0]) {
//		_bidTeam = @"teamA";
//	} else {
//		_bidTeam = @"teamB";
//	}
	
	
	
	// TRASH
	//_gameTrump = selectedSuit;
	//_gameBid = selectedBidCount;
	
	
	
	
	NSError *error;
	
	
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		
		_bidding_info.bidTeam = _bidTeam;
		_bidding_info.bidSuit = selectedSuit;
		_bidding_info.bidTeamName = _selectedTeamName;
		_bidding_info.bidNumIndex = [NSNumber numberWithInt: bidNumIndex];
		_bidding_info.bidSuitIndex = [NSNumber numberWithInt: bidSuitIndex];
		_bidding_info.bidNum = [NSNumber numberWithInt: [selectedBidCount intValue]];
		
	} else {
		
		Bidding *bid = [NSEntityDescription
						insertNewObjectForEntityForName:@"Bidding"
						inManagedObjectContext:_managedObjectContext];
		
		bid.bidTeam = _bidTeam;
		bid.bidSuit = selectedSuit;
		bid.bidTeamName = _selectedTeamName;
		bid.bidNumIndex = [NSNumber numberWithInt: bidNumIndex];
		bid.bidSuitIndex = [NSNumber numberWithInt: bidSuitIndex];
		bid.bidNum = [NSNumber numberWithInt: [selectedBidCount intValue]];
		
		[self fetchBiddingResults];
		_bidding_info = [_biddingData objectAtIndex:0];
	}
	
	
	
	
	[_managedObjectContext save:&error];
	
	
	
	
	NSLog(@"\n\n");
	NSLog(@"Bid Team: You selected %@", _bidTeam);
	NSLog(@"Team Name: %@",[[NSString alloc] initWithFormat:@"you selected %@", _selectedTeamName]);
	NSLog(@"Suit: %@",[[NSString alloc] initWithFormat:@"you selected %@", selectedSuit]);
	NSLog(@"Bid: %@",[[NSString alloc] initWithFormat:@"you selected %@", selectedBidCount]);
	NSLog(@"\n\n");
	
	
	
	NSLog(@"selected row:  %ld", (long)[_biddingPickerView selectedRowInComponent:1]);
	
	
	[self dismissViewControllerAnimated:YES completion:nil];

	
	
// TRASH
//	[self hidePickerView:YES];
//	
//	
//	[self setupGameBid];
//	
//	
//	[self showXYPos:_biddingPopUpView];
}

-(void)fetchBiddingResults {
	_biddingData = [FetchDataArray dataFromEntity:@"Bidding" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}


-(IBAction)segmentValueChanged:(UISegmentedControl *)sender {
	
	if (sender == nil) {
		_selectedTeamName = [_segmentTeams titleForSegmentAtIndex:0];
		_bidTeam = @"teamA";
		return;
	}
	
	switch (sender.selectedSegmentIndex) {
		case 0:
			_selectedTeamName = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
			_bidTeam = @"teamA";
			break;
		case 1:
			_selectedTeamName = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
			_bidTeam = @"teamB";
			break;
	}
	NSLog(@"Title: %@",[sender titleForSegmentAtIndex:sender.selectedSegmentIndex]);
}


-(void)setupTeamNamesGameType
{
	if (_teamNameA == nil) {
		_teamNameA = [[_teamData valueForKey:@"teamName"] objectAtIndex:0];
		_teamNameB = [[_teamData valueForKey:@"teamName"] objectAtIndex:1];
	}
	
	[_segmentTeams setTitle:_teamNameA forSegmentAtIndex:0];
	[_segmentTeams setTitle:_teamNameB forSegmentAtIndex:1];
	
	NSLog(@"teamnamea: %@",_teamNameA);
	NSLog(@"teamnameb: %@",_teamNameB);
}


#pragma mark - Picker Config
-(void)loadPickerData {
	
	//	PickerView *picker
	
	NSLog(@"_biddingPickerView %@", _biddingPickerView);
	
	
	NSLog(@"pickerData()");
	
	

	
	NSArray *tmpSuitData = [[NSArray alloc] initWithObjects:
						 @"Spades",
						 @"Clubs",
						 @"Diamonds",
						 @"Hearts",
						 @"No Trump",
						 nil];
	_pickerSuitArray = tmpSuitData;
	
	
	NSArray *tmpBidData = [[NSArray alloc] initWithObjects:
						   @"6",
						   @"7",
						   @"8",
						   @"9",
						   @"10",
						   nil];
	_pickerBidArray = tmpBidData;
}



#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 2;
}

// returns the # of rows in each component..
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	
	switch (component) {
			
		case 1:
			return [_pickerBidArray count];
			break;
		default:
		case 0:
			return [_pickerSuitArray count];
			break;
			
	}
	
	NSLog(@"");
}


#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	
	switch (component) {
		case 0:
			return [_pickerSuitArray objectAtIndex:row];
			break;
		case 1:
			return [_pickerBidArray objectAtIndex:row];
			break;
	}
	
	NSLog(@"");
	return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	
	NSLog(@"Selected Row %ld", (long)row);
	
	switch(row) {
			
	}
	
	
	
	
	NSLog(@"\n");
	NSLog(@"\n");
}

-(void)resetPickerSelection:(int)suitIndex bidIndex:(int)bidIndex {
	
//	NSLog(@"teamIndex %i", teamIndex);
	NSLog(@"suitIndex %i", suitIndex);
	NSLog(@"bidNumIndex %i", bidIndex);
	
	NSLog(@"\n");
	
//	[_teamPickerView selectRow:teamIndex inComponent:0 animated:NO];
	[_biddingPickerView selectRow:suitIndex inComponent:0 animated:NO];
	[_biddingPickerView selectRow:bidIndex inComponent:1 animated:NO];
	
}



-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {

	switch (component){
		case 0:
			return 130.0f;
		case 1:
			return 40.0f;
	}
	
	return 0;
}

#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		// Move the start/stop button up...
		//		float xPos = _btns_start_stop.center.x;
		//		float yPos = _btns_start_stop.center.y;
		
		//		float xPosSettings = _btn_settings.center.x;
		//		float yPosSettings = _btn_settings.center.y;
		
		//		yPos = yPos - 50;
		//		yPosSettings = yPosSettings - 10;
		
		
		
		// Move the view up
		[UIView animateWithDuration:0.2
						 animations:^{
							 
							 _contentView.frame = CGRectMake(
															 _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height - _bannerView.frame.size.height));
							 
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the start/stop button back down...
		[UIView animateWithDuration:0.2
						 animations:^{
							 _contentView.frame = CGRectMake(
															 _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height + _bannerView.frame.size.height));
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}


@end
