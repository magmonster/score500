//
//  ScoringViewController.h
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Data
#import "Team.h"
#import "Hand.h"
#import "Trump.h"
#import "Setup.h"
#import "Bidding.h"

// Utilities
#import "Colors.h"
#import "Constants.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "BSKeyboardControls.h"

// Banners
@class GADBannerView;
@import GoogleMobileAds;


@class ScoringViewController;


@interface ScoringViewController : UIViewController <GADBannerViewDelegate, UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate> {
		
	CGFloat animatedDistance;
	BOOL boolBiddingTeamSet;
}

@property (strong, nonatomic) IBOutlet UIControl *view_backPanel;


-(void)bannerConfig;

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;




@property (strong, nonatomic) NSString *scoreType;

-(IBAction)dismissModal:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *borderTeam1Tricks;

@property (strong, nonatomic) IBOutlet UIView *borderTeam2Tricks;


// Local Data
@property (strong, nonatomic) NSArray *teamData;

@property (strong, nonatomic) Trump *trump_info;
@property (strong, nonatomic) NSMutableArray *trumpData;

@property (strong, nonatomic) Setup *setup_info;
@property (strong, nonatomic) NSArray *setupData;

@property (strong, nonatomic) Bidding *bidding_info;
@property (strong, nonatomic) NSArray *biddingData;

@property (strong, nonatomic) Hand *selected_hand;
@property (strong, nonatomic) NSMutableArray *handData;




@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



@property (strong, nonatomic) IBOutlet UILabel *lblTeamA;
@property (strong, nonatomic) IBOutlet UILabel *lblTeamB;
@property (strong, nonatomic) NSString *teamNameA;
@property (strong, nonatomic) NSString *teamNameB;

@property (strong, nonatomic) IBOutlet UITextField *txtTeamTricksA;
@property (strong, nonatomic) IBOutlet UITextField *txtTeamTricksB;


@property (strong, nonatomic) IBOutlet UILabel *lblTeamScoreA;
@property (strong, nonatomic) IBOutlet UILabel *lblTeamScoreB;


@property (strong, nonatomic) IBOutlet UIImageView *imgBidIndicatorA;
@property (strong, nonatomic) IBOutlet UIImageView *imgBidIndicatorB;
@property (strong, nonatomic) IBOutlet UIImageView *imgScoreIndicatorA;
@property (strong, nonatomic) IBOutlet UIImageView *imgScoreIndicatorB;


@property (strong, nonatomic) IBOutlet UILabel *lblStatusMessage;




@property (strong, nonatomic) NSString *bidTrump;
@property (strong, nonatomic) NSString *bidNumber;
@property (strong, nonatomic) NSString *bidTeam;
@property (strong, nonatomic) NSString *biddingTeamName;






// organize
@property (strong, nonatomic) NSString *winningTeam;





@property (assign) int editRowIndex;
@property (strong, nonatomic) NSString *editScoreA;
@property (strong, nonatomic) NSString *editScoreB;


@property (strong, nonatomic) IBOutlet UIButton *btnSaveScore;

@property (assign) int teamAPoints;
@property (assign) int teamBPoints;


@property (strong, nonatomic) IBOutlet UIView *starBidBackgroundA;
@property (strong, nonatomic) IBOutlet UIView *starBidBackgroundB;


@property (strong, nonatomic) IBOutlet UILabel *lblBiddingTeam;

@property (strong, nonatomic) IBOutlet UILabel *lblTheBid;

-(IBAction)saveScore:(id)sender;
-(IBAction)editingDidBegin:(id)sender;






// used for interacting with the keyboard, and moving the view
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
-(IBAction)dismissKeyboard:(id)sender;
-(void)setupKeyboardControls;
-(void)scrollViewToTextField:(id)textField;


@end
