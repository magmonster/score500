//
//  BiddingViewController.m
//  Score500
//
//  Created by Drew Bombard on 12/30/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import "BiddingViewController.h"

@interface BiddingViewController ()

@end

@implementation BiddingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)dismissModal:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
