//
//  GameViewController.m
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()

@end

@implementation GameViewController






- (void)viewDidLoad
{
	[super viewDidLoad];
	

	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	
//	AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//	appDelegate.gameViewController = self;
//	NSManagedObjectContext *context = [appDelegate managedObjectContext];
//	[context setUndoManager:nil];
//	_managedObjectContext = context;

	
	_screenHeight = appDelegate.screenHeight;
	_screenWidth = appDelegate.screenWidth;
	
	
	
	
	[self customizeInterface];
	
	[self disableScoringButton];
	

	
	
	
	
	
	
	
	
	/**
	 * Check for local data.  If there is some,
	 * (and there should be) then grab the data.
	 */
	if ([CoreDataCheck checkEntity:@"Setup"] > 0) {
		[self fetchSetupResults];
		_setup_info = [_setupData objectAtIndex:0];
	}
	
	/**
	 * Check for local data.  If there is some,
	 * (and there should be) then grab the data.
	 */
	
	
	// Moved to willAppear
	//	[self setupTeamNamesGameType];
	
	//	[self loadPickerData];
	
	//	[self setupBiddingData];
	
	//	[self checkViewCompletion];
	
	//	[self.tableView reloadData];
	
	
	
	
	
	NSLog(@"\n");
	NSLog(@"_gameType: %@",_gameType);
	NSLog(@"_gameRounds: %@",_gameRounds);
}

-(void)viewDidUnload
{
	[super viewDidUnload];
	self.fetchedResultsController = nil;
}

-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
	
	NSLog(@"\n\n");
	NSLog(@"GameViewController: viewWillAppear()");
	
	
	[super viewWillAppear:animated];
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
	
	
	
	
	[self getSomeAwesomeData];
	
	

	
	
	[self setupBiddingData];
	
	[self setupTeamNamesGameType];
	
	[self checkViewCompletion];
	
	
	
	[self.tableView reloadData];
	
	[self calculateScore];
	
	//	[self.tableView reloadData];
	
}







-(void)showXYPos:(UIView *)requestedView {
	
	NSLog(@"\n");
	NSLog(@"X: %f", requestedView.frame.origin.x);
	NSLog(@"Y: %f", requestedView.frame.origin.y);
	NSLog(@"\n");
}











#pragma mark - Boilerplate stuff from Apple/Ray Wenderlich
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	UITableView *tableView = self.tableView;
 
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeUpdate:
			[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			break;
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray
											   arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:[NSArray
											   arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeMove:
			NSLog(@"A table item was moved");
			break;
		case NSFetchedResultsChangeUpdate:
			NSLog(@"A table item was updated");
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}




- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		
		[self.tableView beginUpdates]; // Avoid  NSInternalInconsistencyException
		
		// Delete the role object that was swiped
		Hand *handToDelete = [_fetchedResultsController objectAtIndexPath:indexPath];
		
		[_managedObjectContext deleteObject:handToDelete];
		[_managedObjectContext save:nil];
		
		
		// Delete the (now empty) row on the table
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
							  withRowAnimation:UITableViewRowAnimationFade];
		
		
		
		NSLog(@"count: %lu", (unsigned long)[_fetchedResultsController.fetchedObjects count]);
		
		
		
		
		if (!_fetchedResultsController ||
			[_fetchedResultsController.fetchedObjects count] == 0) {
			
			// Do something..
		}
		
		[self.tableView endUpdates];

		[self calculateScore];
	}
}

#pragma mark - Customize TableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 8.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headerView = [[UIView alloc] init];
	headerView.backgroundColor = [UIColor clearColor];
	return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}


#pragma mark - TableView Setup and Layout
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//return 1;
	
	id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	//return [_handData count];
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
    static NSString *cellIdentifier = @"Cell";
    ScoringCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if (cell == nil) {
		cell = [[ScoringCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = [UIColor colorWithWhite:0.980 alpha:0.890];
	} else {
		cell.backgroundColor = [UIColor whiteColor];
	}
	
	
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}


-(void)configureCell:(ScoringCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	NSLog(@"\n\n\nconfigureCell()");
	
	NSLog(@"indexPath for cell: %@", indexPath);
	NSLog(@"ScoringCell : %@", cell);
	NSLog(@"\n");
	
	
	// Configure the cell...
	Hand *hand_info = [_fetchedResultsController objectAtIndexPath:indexPath];

	NSLog(@"_fetchedResultsController count: %lu", (unsigned long)[_fetchedResultsController.fetchedObjects count]);

	
	
	cell.lblScoreA.text = [NSString stringWithFormat:@"%@",hand_info.scoreTeamA];
	cell.lblScoreB.text = [NSString stringWithFormat:@"%@",hand_info.scoreTeamB];
	
	NSLog(@"score A (%ld): %@", (long)indexPath.section, [NSString stringWithFormat:@"%@",hand_info.scoreTeamA]);
	NSLog(@"score B (%ld): %@", (long)indexPath.section, [NSString stringWithFormat:@"%@",hand_info.scoreTeamB]);
	NSLog(@"\n");
	NSLog(@"\n");
	
	

	
	/**
	 * Check row value and color appropirately;
	 */
	UIColor *cellTintA;
	UIColor *cellTintB;
	
	if ([hand_info.scoreTeamA intValue]  < 0 ) {
		cellTintA = [Colors get].darkRed;
	} else {
		cellTintA = [Colors get].darkGray;
	}
	
	if ([hand_info.scoreTeamB intValue]  < 0 ) {
		cellTintB = [Colors get].darkRed;
	} else {
		cellTintB = [Colors get].darkGray;
	}
	
	cell.lblScoreA.textColor = cellTintA;
	cell.lblScoreB.textColor = cellTintB;
}




#pragma mark - Data Retreival (fetchedResultsController)
-(NSFetchedResultsController *)fetchedResultsController {
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Hand"
								   inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"sortOrder"
							  ascending:YES
							  selector:@selector(compare:)];
	
	// Sort it
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	[fetchRequest setFetchBatchSize:20];
	
	// Setup and fetch
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	// Set results
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	return _fetchedResultsController;
}











-(void)enableScoringButton {

	_bidTeamReady = 1;
	
	_btnAddScore.enabled = YES;
	_btnAddScore.hidden = NO;
		
	_btnAddScore.backgroundColor = [Colors get].medOrange;
	_btnAddScore.titleLabel.textColor = [UIColor whiteColor];
	[_btnAddScore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[_btnAddScore setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
	[_btnAddScore setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	
// TRASH
//	_viewGameBidItems.backgroundColor = [Colors get].medBlue;
}
-(void)disableScoringButton {

	_bidTeamReady = 0;
	
	_btnAddScore.enabled = NO;
	_btnAddScore.hidden = YES;
	
	_btnAddScore.backgroundColor = [Colors get].lightGray;
	_btnAddScore.titleLabel.textColor = [Colors get].darkGray;
	[_btnAddScore setTitleColor:[Colors get].darkGray forState:UIControlStateNormal];
	[_btnAddScore setTitleColor:[Colors get].darkGray forState:UIControlStateSelected];
	[_btnAddScore setTitleColor:[Colors get].darkGray forState:UIControlStateHighlighted];
}

-(IBAction)addScore:(id)sender {

	NSLog(@"addScore");
	if (_bidTeamReady == 1) {
		[self performSegueWithIdentifier:@"addScoringSegue" sender:nil];
	} else {
		[self alertNoBid];
	}
	
}

















- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	NSLog(@"Let's move, we are in the segue....");
	
	
	int selectedIndex = (int)[[self.tableView indexPathForSelectedRow] row];
	
	
	if ([segue.identifier isEqualToString:@"addScoringSegue"]) {
		
		NSLog(@"addScoringSegue");
		NSLog(@"\n\n");
		
NSLog(@"_bidding_info: %@", _bidding_info);

		UINavigationController *nav = segue.destinationViewController;
		ScoringViewController *scoringVC = (ScoringViewController *)nav.topViewController;
		scoringVC.scoreType  = @"new";
		
		
		//ScoringViewController *scoringVC = segue.destinationViewController;
		scoringVC.bidNumber = [_bidding_info.bidNum stringValue];
		scoringVC.bidTrump = _bidding_info.bidSuit;
		scoringVC.bidTeam = _bidding_info.bidTeam;
		scoringVC.teamNameA = _teamNameA;
		scoringVC.teamNameB = _teamNameB;
		
		scoringVC.scoreType = @"new";

// TRASH ?!?
//		scoringVC.delegate = self;

		NSLog(@"\n\n");
		
	} else if ([segue.identifier isEqualToString:@"editScoringSegue"]) {
		
		NSLog(@"editScoringSegue");
		NSLog(@"\n\n");
		
		UINavigationController *nav = segue.destinationViewController;
		ScoringViewController *scoringVC = (ScoringViewController *)nav.topViewController;
		scoringVC.scoreType  = @"edit";
		
//		ScoringViewController *scoringVC = segue.destinationViewController;
		scoringVC.bidNumber = _gameBid;
		scoringVC.bidTrump = _gameTrump;
		scoringVC.bidTeam = _bidTeam;
		scoringVC.teamNameA = _teamNameA;
		scoringVC.teamNameB = _teamNameB;
		
		scoringVC.editRowIndex = selectedIndex;

		
		// Configure the cell...
		self.selected_hand = [_fetchedResultsController.fetchedObjects objectAtIndex:selectedIndex];
		scoringVC.selected_hand = self.selected_hand;

	} else {
		NSLog(@"Unidentified Segue Attempted!");
	}

}


-(void)checkViewCompletion {
	
	NSLog(@"\n\n");
	NSLog(@"checkViewCompletion()\n");

	if (
		[_gameTrump length] == 0 &&
		[_gameType length] == 0 &&
		_gameRounds == 0 &&
		[_gameBid length] == 0) {

	}
	
	
	
	
	

	NSLog(@"_gameTrump: %@",_gameTrump);
	NSLog(@"_gameBid: %@",_gameBid);
	NSLog(@"_bidTeam: %@",_bidTeam);
	
	NSLog(@"\n\n\n");
	
		
	NSLog(@"_bidTeamReady: %d",_bidTeamReady);

	
	// Set the star for the team that's bidding
	if (_bidTeam) {
		
		NSLog(@"GO TEAM!!");
		NSLog(@"\n\n\n");
		
		_bidTeamReady = 1;
		[self enableScoringButton];
	}
	
	// Set the bid number in the trump image
	
	NSLog(@"_bidTeamReady: %d",_bidTeamReady);


}














# pragma mark - Animation control(s)
- (void)animateView:(NSString *)animationID
					  finished:(NSNumber *)finished
					   context:(void *)context
			viewToMove:(UIView *)biddingBackground
						 xPos:(float)xPos
						 yPos:(float)yPos {
		
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationDelay:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	
	biddingBackground.center = CGPointMake(xPos, yPos);
	
    [UIView commitAnimations];
}



-(float)findYPos:(UIView *)uiView {

	return uiView.frame.origin.y;
}


#pragma mark - Score Array Calcuation
-(void)calculateScore {
	
	NSLog(@"\n\n");
	NSLog(@"calculateScore()");
	
	NSString *winner;
	int sumA = 0;
	int sumB = 0;
	
	
	Hand *scoring_data;
	for (int i = 0; i < [_fetchedResultsController.fetchedObjects count]; i++) {
		NSLog(@"%d",i);
		scoring_data = [_fetchedResultsController.fetchedObjects objectAtIndex:i];
		
		sumA = sumA + [scoring_data.scoreTeamA intValue];
		sumB = sumB + [scoring_data.scoreTeamB intValue];
	}
	
	
	NSLog(@"sumA %d", sumA);
	NSLog(@"sumB %d", sumB);
	NSLog(@"gameType: %@",_gameType);
	NSLog(@"\n\n");
	NSLog(@"SUM (A) ----> %d", sumA);
	NSLog(@"SUM (B) ----> %d", sumB);
	
	
	if ([_gameType isEqual: @"500"]) {
		
	
		/**
		 * Game over.
		 * At least one of the player has reached 500
		 */
		if ( (sumA >= 500) || (sumB >= 500) ) {
			
			if (sumA > sumB) {
				winner = _teamNameA;
			} else if (sumB > sumA) {
				winner = _teamNameB;
			} else if (sumA == sumB) {
				winner = @"tie";
			}
			
			[self alertWinner:2 winner:winner];
		}
	}
	
	
	else if ([_gameType isEqual: @"rounds"]) {
		
		/**
		 * Game over.
		 * Players have met the number of game rounds
		 */
		
		NSLog(@"_handData count: %lu", (unsigned long)[_handData count]);

		NSLog(@"_gameRounds count: %@", _gameRounds);
		if ( [_handData count] >= [_gameRounds intValue] ) {
			
			if (sumA > sumB) {
				winner = _teamNameA;
			} else if (sumB > sumA) {
				winner = _teamNameB;
			} else if (sumA == sumB) {
				winner = @"tie";
			}
			
			[self alertWinner:3 winner:winner];
		}
	}
	
	
	_lblTotalTeamScoreA.text = [NSString stringWithFormat:@"%d", sumA];
	_lblTotalTeamScoreB.text = [NSString stringWithFormat:@"%d", sumB];
	
	[self.tableView reloadData];
}



#pragma mark - Alert(s)

-(void)alertWinner:(int)alertTag winner:(NSString *)winner {
	
	NSString *alertMessage;
	if ([winner isEqual: @"tie"]) {
		alertMessage = @"It's a TIE!\nStart a new game?";
	} else {
		alertMessage = @"\nClear saved scores, \nand start a new game?";
	}
	
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Game Over"
								 message:alertMessage
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* yesButton = [UIAlertAction
								actionWithTitle:@"New Game"
								style:UIAlertActionStyleDefault
								handler:^(UIAlertAction * action) {
									
									NSLog(@"OK button clicked");
									[self cancelGame];
								}];
	
	UIAlertAction* noButton = [UIAlertAction
							   actionWithTitle:@"Cancel"
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction * action) {
								   NSLog(@"CANCEL button clicked");
							   }];
	
	[alert addAction:yesButton];
	[alert addAction:noButton];
	
	[self presentViewController:alert animated:YES completion:nil];
	
}

-(void)alertNoBid
{
	NSString *alertMessage = @"Before adding your tricks, set a bid for this hand below.";
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Bid has not been set"
								 message:alertMessage
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* yesButton = [UIAlertAction
								actionWithTitle:@"OK"
								style:UIAlertActionStyleDefault
								handler:^(UIAlertAction * action) {
									NSLog(@"OK button clicked");
								}];
	
	[alert addAction:yesButton];
	
	[self presentViewController:alert animated:YES completion:nil];
}






#pragma mark - View Loading / Memory Management





-(void)setupTeamNamesGameType {
	
	if (_teamNameA == nil) {
		_teamNameA = [[_teamData valueForKey:@"teamName"] objectAtIndex:0];
		_teamNameB = [[_teamData valueForKey:@"teamName"] objectAtIndex:1];
	}

	_lblTeamA.text = _teamNameA;
	_lblTeamB.text = _teamNameB;


	_gameType = _setup_info.gameType;
	_gameRounds = _setup_info.numRounds;
	
	
	//*
	// DEBUG Data logs
	NSLog(@"_lblTeamA.text %@",_lblTeamA.text);
	NSLog(@"_lblTeamB.text %@",_lblTeamB.text);

	NSLog(@"_gameRounds %@",_gameRounds);
	NSLog(@"_gameType %@",_gameType);
	
	
	
	NSLog(@"teamnamea: %@",_teamNameA);
	NSLog(@"teamnameb: %@",_teamNameB);
	//*/
}



-(void)setupBidView {
	
	
	NSLog(@"_gameTrump: %@",_gameTrump);
	NSLog(@"_gameBid: %@",_gameBid);
	
	NSLog(@"_bidding_info.bidSuit: %@",_bidding_info.bidSuit);
	NSLog(@"_bidding_info.bidNum: %@",_bidding_info.bidNum);
	NSLog(@"_bidding_info.bidTeam: %@",_bidding_info.bidTeam);
	
	
	NSString *trumpSuit;
	NSString *thisGamesBid;
	
	
	NSString *bidSuit;
	NSNumber *bidNum;
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		bidSuit = _bidding_info.bidSuit;
		bidNum = _bidding_info.bidNum;
	} else {
		bidNum = [NSNumber numberWithInt:[_gameBid intValue]];
		bidSuit = _gameTrump;
	}
	
	NSLog(@"bidSuit: %@",bidSuit);
	NSLog(@"bidNum: %@",bidNum);
	
	if ([bidSuit isEqual: @"No Trump"]) {
		trumpSuit = @"bid_jokers";
		thisGamesBid = [[ [NSString stringWithFormat:@"%@", bidNum] stringByAppendingString:@" "] stringByAppendingString:bidSuit];
	} else {
		trumpSuit = [@"bid_" stringByAppendingString: [bidSuit lowercaseString]];
		thisGamesBid = [[[ [NSString stringWithFormat:@"%@", bidNum] stringByAppendingString:@" "] stringByAppendingString:bidSuit] stringByAppendingString:@"'s"];
	}
	
	NSLog(@"trumpSuit: %@",trumpSuit);

	_imgGameTrump.image = [UIImage imageNamed: trumpSuit];
	_lblGameBid.text = thisGamesBid;
	


	
_viewGameBidItems.hidden = NO;
	
	_btnNoBid.hidden = YES;
	_btnChangeBid.hidden = NO;
	


	
	
	_noBidAlertBubble.hidden = YES;

	
	[self animateCurrentBidder];
	
}


-(void)setupBiddingData {


	
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		[self fetchBiddingResults];
		_bidding_info = [_biddingData objectAtIndex:0];
		
		_bidTeam = _bidding_info.bidTeam;
		_biddingTeamName = _bidding_info.bidTeamName;
		
		
		// Fetch bid indexes and reset picker
		[self resetPickerSelection:[_bidding_info.bidTeamIndex intValue]
						 suitIndex:[_bidding_info.bidSuitIndex intValue]
						  bidIndex: [_bidding_info.bidNumIndex intValue]];
	
	
	[self setupBidView];
	}
	
	

}





-(IBAction)cancelAlert:(id)sender
{
	NSString *alertMessage = @"Clear saved scores and \nstart a new game?";
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Title"
								 message:alertMessage
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* yesButton = [UIAlertAction
								actionWithTitle:@"Yes, please"
								style:UIAlertActionStyleDefault
								handler:^(UIAlertAction * action) {

									NSLog(@"OK button clicked");
									[self cancelGame];
								}];
	
	UIAlertAction* noButton = [UIAlertAction
							   actionWithTitle:@"Cancel"
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction * action) {
								   NSLog(@"CANCEL button clicked");
							   }];
	
	[alert addAction:yesButton];
	[alert addAction:noButton];
	
	[self presentViewController:alert animated:YES completion:nil];
}


-(void)cancelGame {
	
	/*
	 * User has chosen to cancel this game
	 * Cancelling game will remove (most) view items
	 * as well as saved hand and bid Core Data
	 */
	
	
	/*
	 * Delete all records of Bidding activity
	 */
	[self deleteFromEntity:@"Bidding" predicate:nil predicateValue:nil];
	
	/*
	 * Delete all records of Hand activity
	 */
	[self deleteFromEntity:@"Hand" predicate:nil predicateValue:nil];
	
	/*
	 * Clear out view data
	 */
	_lblTotalTeamScoreA.text = @"0";
	_lblTotalTeamScoreB.text = @"0";
	
	// Reset button states
	_btnNoBid.hidden = NO;
	_noBidAlertBubble.hidden = NO;
	_btnChangeBid.hidden = YES;
	
	
	
	_viewGameBidItems.hidden = YES;
	
	[self disableScoringButton];
	
	
	// Reload table view
	[_handData removeAllObjects];
	[self.tableView reloadData];


	
	[self animateView:nil
			 finished:nil
			  context:nil
		   viewToMove:_currentBiddingTeam
				 xPos:_viewTeamNames.frame.size.width/2
				 yPos:_viewTeamNames.frame.size.height-1];

}







-(void)deleteFromEntity:(NSString *)entityName predicate:(NSString *)predicate predicateValue:(NSString *)predicateValue {
	
	/*
	 * Delete all records of passed entity
	 */
	NSLog(@"entityName: %@",entityName);
	NSLog(@"predicate: %@",predicate);
	NSLog(@"predicateValue: %@",predicateValue);
	
	NSFetchRequest *deleteRequest = [[NSFetchRequest alloc] init];
	[deleteRequest setEntity:[NSEntityDescription entityForName:entityName
										 inManagedObjectContext:_managedObjectContext]];
	
	if (predicateValue != nil) {
		// 3 - Filter it (optional)
		deleteRequest.predicate = [NSPredicate predicateWithFormat:[[predicate stringByAppendingString:@" == "] stringByAppendingString:predicateValue]];
	}
	
	//NSLog(@"predicate: %@", deleteRequest.predicate);
	NSArray *deleteResult = [_managedObjectContext executeFetchRequest:deleteRequest error:nil];
	
	
	for (id object in deleteResult) {
		[_managedObjectContext deleteObject:object];
		NSLog(@"Deleting object: %@",object);
	}
	
	// write to database
	[_managedObjectContext save:nil];
}




-(void)startOver {
	
	NSLog(@"startOver()");
	
	[self.tableView reloadData];

	_lblNoBid.hidden = NO;
	_viewGameBidItems.hidden = YES;

	_gameType = NULL;
	
	[self performSegueWithIdentifier:@"setupSegue" sender:nil];
}


-(void)getSomeAwesomeData {
	
	
	/**
	 * Check for local data.  If there is some,
	 * (and there should be) then grab the data.
	 */
	if ([CoreDataCheck checkEntity:@"Setup"] > 0) {

		[self fetchSetupResults];
		_setup_info = [_setupData objectAtIndex:0];
		_bidding_info = [_biddingData objectAtIndex:0];
	}
}




















-(IBAction)hideBid:(id)sender {
	
	NSLog(@"hideBid()");

	
	
//	TRASH
//	[self showXYPos:_biddingPopUpView];
	

	
	
	NSString *selectedTeamName = [_pickerTeamArray objectAtIndex:[_biddingPicker selectedRowInComponent:0]];
	int bidTeamIndex = (int)[_biddingPicker selectedRowInComponent:0];
	
	NSString *selectedSuit = [_pickerSuitArray objectAtIndex:[_biddingPicker selectedRowInComponent:1]];
	int bidSuitIndex = (int)[_biddingPicker selectedRowInComponent:1];
	
	NSString *selectedBidCount = [_pickerBidArray objectAtIndex:[_biddingPicker selectedRowInComponent:2]];
	int bidNumIndex = (int)[_biddingPicker selectedRowInComponent:2];
	

	
	
	if (selectedTeamName == [_pickerTeamArray objectAtIndex:0]) {
		_bidTeam = @"teamA";
	} else {
		_bidTeam = @"teamB";
	}


	_gameTrump = selectedSuit;
	_gameBid = selectedBidCount;
	
	
	
	
	
	
	
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		
		_bidding_info.bidTeam = _bidTeam;
		_bidding_info.bidTeamName = selectedTeamName;
		_bidding_info.bidSuit = selectedSuit;
		_bidding_info.bidNum = [NSNumber numberWithInt: [selectedBidCount intValue]];
		
		_bidding_info.bidNumIndex = [NSNumber numberWithInt: bidNumIndex];
		_bidding_info.bidTeamIndex = [NSNumber numberWithInt: bidTeamIndex];
		_bidding_info.bidSuitIndex = [NSNumber numberWithInt: bidSuitIndex];
		
		[_managedObjectContext save:nil];

	} else {
		Bidding *bid = [NSEntityDescription
						insertNewObjectForEntityForName:@"Bidding"
						inManagedObjectContext:_managedObjectContext];
		
		bid.bidTeam = _bidTeam;
		bid.bidTeamName = selectedTeamName;
		bid.bidSuit = selectedSuit;
		bid.bidNum = [NSNumber numberWithInt: [selectedBidCount intValue]];
		
		bid.bidNumIndex = [NSNumber numberWithInt: bidNumIndex];
		bid.bidTeamIndex = [NSNumber numberWithInt: bidTeamIndex];
		bid.bidSuitIndex = [NSNumber numberWithInt: bidSuitIndex];
		
		[_managedObjectContext save:nil];
		
		[self fetchBiddingResults];
		_bidding_info = [_biddingData objectAtIndex:0];
	}

	
	
	
	NSLog(@"\n\n");
	NSLog(@"Bid Team: You selected %@", _bidTeam);
	NSLog(@"Team Name: %@",[[NSString alloc] initWithFormat:@"you selected %@", selectedTeamName]);
	NSLog(@"Suit: %@",[[NSString alloc] initWithFormat:@"you selected %@", selectedSuit]);
	NSLog(@"Bid: %@",[[NSString alloc] initWithFormat:@"you selected %@", selectedBidCount]);
	NSLog(@"\n\n");


	
	NSLog(@"selected row:  %ld", (long)[_biddingPicker selectedRowInComponent:1]);
	

	
// TRASH
//	[self hidePickerView:YES];
	
		
	[self setupGameBid];

	
//	TRASH
//	[self showXYPos:_biddingPopUpView];
}







-(void)setupGameBid {
    
	NSLog(@"setupGameBid()");
	
	NSLog(@"_gameTrump: %@",_gameTrump);
	NSLog(@"_gameBid: %@",_gameBid);
	NSLog(@"_bidTeam: %@",_bidTeam);
	
	
	NSLog(@"Records found: %i", [CoreDataCheck checkEntity:@"Team"]);
	
	if ([CoreDataCheck checkEntity:@"Bidding"] == 0) {
		[FetchDataArray dataFromEntity:@"Bidding" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	}
	

	
	if (_bidTeam == nil || _gameTrump == nil || [_gameBid  isEqual: @""]) {
		
		/**
		 * If the user came back to the game screen without setting
		 * up a bid, stop game action and throw an alert.
		 */
		[self alertNoBid];
		
		
	} else {
		
		
		[self checkViewCompletion];
		[self setupBidView];
		
		[self.tableView reloadData];
		
	}
}



-(void)animateCurrentBidder {
	
	NSLog(@"_bidTeam: %@",_bidTeam);
	
	NSLog(@"X: %f",
		 _viewTeam1.frame.origin.x);
	
	
	float moveToX;
	
	if ([_bidTeam isEqual: @"teamA"]) {
		moveToX = _viewTeam1.frame.origin.x + (_viewTeam1.frame.size.width/2);
	} else {
		moveToX = _viewTeam2.frame.origin.x + (_viewTeam2.frame.size.width/2);
	}
	
	[self animateView:nil
			 finished:nil
			  context:nil
		   viewToMove:_currentBiddingTeam
				 xPos:moveToX
				 yPos:_viewTeamNames.frame.size.height];
}










// TRASH

//-(IBAction)showPickerView:(id)sender {
//	
//	[self showXYPos:_biddingPopUpView];
//	
//	NSLog(@"_screenWidth: %f",_screenWidth);
//	NSLog(@"_screenHeight: %f",_screenHeight);
//	
//	
//	_pickerBackgroundBlur.hidden = NO;
//	
//	[UIView beginAnimations:nil context:nil];
//	[UIView setAnimationDuration:0.15];
//	[UIView setAnimationDelay:0.0];
//	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//	[UIView setAnimationDelegate:self];
//	
//	
//	CGRect frame = _biddingPopUpView.frame;
//	frame.origin.x = 0;// - frame.size.width;
//	frame.origin.y = (_screenHeight - _biddingPopUpView.frame.size.height);
//	_biddingPopUpView.frame = frame;
//	
//	
//	
//	[self setupBiddingData];
//	
//	NSLog(@"bidding_info: %@",_bidding_info);
//	
//	
//    [UIView commitAnimations];
//	
//	
//	[self showXYPos:_biddingPopUpView];
//}

//-(void)hidePickerView:(BOOL)animateClosing {
//	
//	NSLog(animateClosing ? @"Yes" : @"No");
//	
//	_pickerBackgroundBlur.hidden = YES;
//
//	NSLog(@"_screenWidth: %f",_screenWidth);
//	NSLog(@"_screenHeight: %f",_screenHeight);
//
//	if (animateClosing == YES) {
//		[UIView beginAnimations:nil context:NULL];
//		[UIView setAnimationDuration:0.25];
//		[UIView setAnimationDelay:0.0];
//		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//		[_biddingPopUpView setFrame:CGRectMake(0.0f,
//											   _screenHeight,
//											   _screenWidth,
//											   200.0f)];
//		[UIView commitAnimations];
//		
//	} else {
//		[_biddingPopUpView setFrame:CGRectMake(0.0f,
//											   _screenHeight,
//											   _screenWidth,
//											   200.0f)];
//	}
//}

-(void)customizeInterface {
	
	
	_viewGameBidItems.backgroundColor = [Colors get].medBlue;
	
	
	
	NSLog(@"yof current bid team: %f", _currentBiddingTeam.frame.origin.y);
	
	//	_currentBiddingTeam.frame.size.height + _viewTeamNames.frame.size.height * 2;
	
	_currentBiddingTeam.center = CGPointMake(_viewTeamNames.frame.size.width/2, _viewTeamNames.frame.size.height-1);
	
	//	biddingBackground.center = CGPointMake(xPos, yPos);

	
	
	
	// Create a mask layer and the frame to determine what will be visible in the view.
	CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
	CGRect maskRect = CGRectMake(0, 0, _viewTeamNames.frame.size.width, _viewTeamNames.frame.size.height-0.5);
	
	// Create a path with the rectangle in it.
	CGPathRef path = CGPathCreateWithRect(maskRect, NULL);
	
	// Set the path to the mask layer.
	maskLayer.path = path;
	
	// Release the path since it's not covered by ARC.
	CGPathRelease(path);
	
	// Set the mask of the view.
	_viewTeamNames.layer.mask = maskLayer;
	
	
	
	
	
	CALayer *bottomBorder = [CALayer layer];
	bottomBorder.frame = CGRectMake(0.0f, _viewTeamNames.frame.size.height-1, _viewTeamNames.frame.size.width, 0.6f);
	bottomBorder.backgroundColor = [[Colors get] gray999].CGColor;
//	bottomBorder.backgroundColor = (__bridge CGColorRef _Nullable)([Colors get].gray999);
	[_viewTeamNames.layer addSublayer:bottomBorder];
	
	
	_noBidAlertBubble.layer.cornerRadius = 11.0f;
	
	
	_viewBtnReset.layer.borderWidth = 0.5;
	_viewBtnReset.layer.borderColor = [[[Colors get] gray999] CGColor];
	
	_viewBtnReset.layer.cornerRadius = _viewBtnReset.frame.size.height/2;
	_viewBtnReset.layer.masksToBounds = YES;
	

	_btnNoBid.layer.borderWidth = 0.5;
	_btnNoBid.layer.borderColor = [[[Colors get] gray999] CGColor];
	
	_btnNoBid.layer.cornerRadius = _btnNoBid.frame.size.height/2;
	_btnNoBid.layer.masksToBounds = YES;
	
	_btnChangeBid.layer.cornerRadius = _btnChangeBid.frame.size.height/2;
	_btnChangeBid.layer.masksToBounds = YES;
	
	
	
	_btnAddScore.layer.cornerRadius = _btnAddScore.frame.size.height/2;
	_btnAddScore.layer.masksToBounds = YES;
	
	
	
	_viewGameBidItems.layer.cornerRadius = 20.0;
	_viewGameBidItems.layer.masksToBounds = YES;

	
	CAGradientLayer *gradientLayer = [CAGradientLayer layer];
	gradientLayer.frame = _gradientMask.bounds;
	gradientLayer.colors = [NSArray arrayWithObjects:
							(id)[UIColor whiteColor].CGColor,
							(id)[UIColor clearColor].CGColor, nil];
	gradientLayer.startPoint = CGPointMake(1.0f, 1.0f);
	gradientLayer.endPoint = CGPointMake(1.0f, 0.0f);
	_gradientMask.layer.mask = gradientLayer;


	
	_btnNoBid.backgroundColor = [Colors get].lightGray;
	_btnNoBid.titleLabel.textColor = [Colors get].darkGray;
	[_btnNoBid setTitleColor:[Colors get].darkGray forState:UIControlStateNormal];
	[_btnNoBid setTitleColor:[Colors get].darkGray forState:UIControlStateSelected];
	[_btnNoBid setTitleColor:[Colors get].darkGray forState:UIControlStateHighlighted];
	
	_btnHideBid.backgroundColor = [Colors get].darkGreen;
	_btnHideBid.titleLabel.textColor = [Colors get].darkGray;
	[_btnHideBid setTitleColor:[Colors get].darkGray forState:UIControlStateNormal];
	[_btnHideBid setTitleColor:[Colors get].darkGray forState:UIControlStateSelected];
	[_btnHideBid setTitleColor:[Colors get].darkGray forState:UIControlStateHighlighted];

	
_viewGameBidItems.hidden = YES;
	
// TRASH
//	[self hidePickerView:NO];
}



-(void)saveSetupData {
	
	NSError *error;
	[self.managedObjectContext save:&error];
	NSLog(@"\n\n");
}




-(void)fetchSetupResults {
	_teamData = [FetchDataArray dataFromEntity:@"Team" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	_setupData = [FetchDataArray dataFromEntity:@"Setup" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}

-(void)fetchBiddingResults {
	_biddingData = [FetchDataArray dataFromEntity:@"Bidding" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}














-(void)homeScreenAlert {
	
	NSLog(@"\n");
	[self cancelAlert:nil];
	//_appDelegate.homeScreenRestart = NO;
}









-(void)loadPickerData {
	
//	PickerView *picker
	
	NSLog(@"_biddingPicker %@", _biddingPicker);
	
	
	NSLog(@"pickerData()");
	
	
	NSArray *tmpTeamData = [[NSArray alloc] initWithObjects:
							_teamNameA,
							_teamNameB,
							nil];
	_pickerTeamArray = tmpTeamData;
	
	
	NSArray *tmpSuitData = [[NSArray alloc] initWithObjects:
						 @"Spades",
						 @"Clubs",
						 @"Diamonds",
						 @"Hearts",
						 @"No Trump",
						 nil];
	_pickerSuitArray = tmpSuitData;
	
	
	NSArray *tmpBidData = [[NSArray alloc] initWithObjects:
						@"6",
						@"7",
						@"8",
						@"9",
						@"10",
						nil];
	_pickerBidArray = tmpBidData;
}


#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 3;
}

// returns the # of rows in each component..
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	
	switch (component) {
		case 1:
			return [_pickerSuitArray count];
			break;
		case 2:
			return [_pickerBidArray count];
			break;
		default:
		case 0:
			return [_pickerTeamArray count];
			break;
	}
}


#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	switch (component) {
		case 0:
			return [_pickerTeamArray objectAtIndex:row];
			break;
		case 1:
			return [_pickerSuitArray objectAtIndex:row];
			break;
		case 2:
			return [_pickerBidArray objectAtIndex:row];
			break;
	}
	return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	
	NSLog(@"Selected Row %ld", (long)row);
    
	switch(row) {
			
	}
	

	
	
	NSLog(@"\n");
	NSLog(@"\n");
}

-(void)resetPickerSelection:(int)teamIndex suitIndex:(int)suitIndex bidIndex:(int)bidIndex {
	
	NSLog(@"teamIndex %i", teamIndex);
	NSLog(@"suitIndex %i", suitIndex);
	NSLog(@"bidNumIndex %i", bidIndex);
	
	NSLog(@"\n");

	[_biddingPicker selectRow:teamIndex inComponent:0 animated:NO];
	[_biddingPicker selectRow:suitIndex inComponent:1 animated:NO];
	[_biddingPicker selectRow:bidIndex inComponent:2 animated:NO];

}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	switch (component){
		case 0:
			return 100.0f;
		case 1:
			return 130.0f;
		case 2:
			return 40.0f;
	}
	return 0;
}




- (IBAction)showSetup:(id)sender {
	[self.tabBarController setSelectedIndex:1];
}
@end
