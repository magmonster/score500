//
//  AppDelegate.m
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "AppDelegate.h"





#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
//#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
//#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
//#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
//#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )


@implementation AppDelegate




+(AppDelegate*)app {
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	

	[FIRApp configure];

	
#ifdef LITE
	_isFreeVersion = YES;
#else
	_isFreeVersion = NO;
#endif
	
	[self customizeInterface];
	
	//instantiate local context
	NSManagedObjectContext *context = [self managedObjectContext];
	if (!context) {
		// Handle the error.
		NSLog(@"Error: Context is null");
	}
	
	 // Override point for customization after application launch.
	return YES;
	
	
	

/*


	// Override point for customization after application launch.
	BOOL shouldPerformAdditionalDelegateHandling = YES;

	// Check API availiability
	// UIApplicationShortcutItem is available in iOS 9 or later.
	if([[UIApplicationShortcutItem class] respondsToSelector:@selector(new)]) {
		
		[self configDynamicShortcutItems];
		
		// If a shortcut was launched, display its information and take the appropriate action
		UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKeyedSubscript:UIApplicationLaunchOptionsShortcutItemKey];
		
		
		if (shortcutItem) {
			// When the app launch at first time, this block can not called.
			[self handleShortCutItem:shortcutItem];
			
			// This will block "performActionForShortcutItem:completionHandler" from being called.
			shouldPerformAdditionalDelegateHandling = NO;
		} else {
			// normal app launch process without quick action
			[self launchWithoutQuickAction];
		}
		
		
	} else {
		
		// Less than iOS9 or later
		[self launchWithoutQuickAction];
		
	}
	
	
	return shouldPerformAdditionalDelegateHandling;
*/


}



-(void)launchWithoutQuickAction {

	_homeScreenRestart = NO;
	
//	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//	
//	FirstViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"firstVC"];
//	
//	self.window.rootViewController = vc;
//	
//	[self.window makeKeyAndVisible];
	
}


/**
 *  @brief config dynamic shortcutItems
 *  @discussion after first launch, users can see dynamic shortcutItems
 */
- (void)configDynamicShortcutItems {
	
	// config image shortcut items
	// if you want to use custom image in app bundles, use iconWithTemplateImageName method
	//UIApplicationShortcutIcon *shortcutResetIcon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeShuffle];
	
	UIApplicationShortcutIcon *shortcutResetIcon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"restart"];


	// TRASH
	//UIApplicationShortcutIcon *shortcutFavoriteIcon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeFavorite];
	
	UIApplicationShortcutItem *shortcutReset = [[UIApplicationShortcutItem alloc]
												 initWithType:@"com.defaultmethod.Score500.Reset"
												 localizedTitle:@"Start Over"
												 localizedSubtitle:@"... and reset scores?"
												 icon:shortcutResetIcon
												 userInfo:nil];
	
//	UIApplicationShortcutItem *shortcutFavorite = [[UIApplicationShortcutItem alloc]
//												   initWithType:@"com.sarangbang.QuickAction.Favorite"
//												   localizedTitle:@"Ummm...."
//												   localizedSubtitle:@"asdfasdf"
//												   icon:shortcutFavoriteIcon
//												   userInfo:nil];
//	
	
	// add all items to an array
	NSArray *items = @[shortcutReset];
	
	// add the array to our app
	[UIApplication sharedApplication].shortcutItems = items;
}


/*
 Called when the user activates your application by selecting a shortcut on the home screen, except when
 application(_:,willFinishLaunchingWithOptions:) or application(_:didFinishLaunchingWithOptions) returns `false`.
 You should handle the shortcut in those callbacks and return `false` if possible. In that case, this
 callback is used if your application is already launched in the background.
 */

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler{
	
	BOOL handledShortCutItem = [self handleShortCutItem:shortcutItem];
	
	completionHandler(handledShortCutItem);
}


/**
 *  @brief handle shortcut item depend on its type
 *
 *  @param shortcutItem shortcutItem  selected shortcut item with quick action.
 *
 *  @return return BOOL description
 */
- (BOOL)handleShortCutItem : (UIApplicationShortcutItem *)shortcutItem {
	
	BOOL handled = NO;
	
	NSString *bundleId = [NSBundle mainBundle].bundleIdentifier;
	
	NSString *shortcutReset = [NSString stringWithFormat:@"%@.Reset", bundleId];

// TRASH
//	NSString *shortcutFavorite = [NSString stringWithFormat:@"%@.Favorite", bundleId];
	
// TRASH
//	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	
	NSLog(@"bundleId: %@",bundleId);
	NSLog(@"shortcutItem.type: %@",shortcutItem.type);
	
	if ([shortcutItem.type isEqualToString:shortcutReset]) {
		handled = YES;
		
		_homeScreenRestart = YES;
		
		
//		SecondViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"secondVC"];
//		
//		self.window.rootViewController = vc;
//		
//		[self.window makeKeyAndVisible];
		
	}

	
//	else if ([shortcutItem.type isEqualToString:shortcutFavorite]) {
//		handled = YES;
//		
//		ThirdViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"thirdVC"];
//		
//		self.window.rootViewController = vc;
//		
//		[self.window makeKeyAndVisible];
//	}
	
	
	return handled;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
	
	NSLog(@"\n");
	
//	UITabBarController *tabb = (UITabBarController *)self.window.rootViewController;
//	tabb.selectedIndex = 0;
// [[NSNotificationCenter defaultCenter] postNotificationName:@"localNotificationReceived" object:nil];
}







-(void)forceCrash {
//	[[Crashlytics sharedInstance] crash];
}


-(void)defineHeightWidth {
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	_screenWidth = screenRect.size.width;
	_screenHeight = screenRect.size.height;
}

-(void)defineDeviceSize {
	
	UIDevice *device = [UIDevice currentDevice];
	//UIDeviceOrientation currentOrientation = device.orientation;
	BOOL isPhone = (device.userInterfaceIdiom == UIUserInterfaceIdiomPhone);
	
	//	BOOL isTallPhone = ([[UIScreen mainScreen] bounds].size.height == 568.0);
	//	BOOL isPhoneLegacy = ([[UIScreen mainScreen] bounds].size.height == 480.0);
	//	BOOL isPhone5 = ([[UIScreen mainScreen] bounds].size.height == 568.0);
	//	BOOL isPhone6 = ([[UIScreen mainScreen] bounds].size.height == 667.0);
	//	BOOL isPhone6Plus = ([[UIScreen mainScreen] bounds].size.height == 736.0);
	
	NSLog(@"mainScreen height: %f", [[UIScreen mainScreen] bounds].size.height);
	NSLog(@"mainScreen width: %f", [[UIScreen mainScreen] bounds].size.width);
	
	int heightComparison = [[UIScreen mainScreen] bounds].size.height;
	NSLog(@"heightComparison: %d", heightComparison);

//	if (UIDeviceOrientationIsPortrait(currentOrientation) == YES) {
		
	// Do Portrait Things
	if (isPhone == YES) {
		
		// Do Portrait Phone Things
		switch (heightComparison) {
			case 480:
				NSLog(@"Legacy Phone");
				_deviceHeight = 480;
				_tutorialSizeModifier = 30;
				break;
			case 568:
				NSLog(@"iPhone 5");
				_deviceHeight = 568;
				_tutorialSizeModifier = 52;
				break;
			case 667:
				NSLog(@"iPhone 6");
				_deviceHeight = 667;
				_tutorialSizeModifier = 52;
				break;
			case 736:
				NSLog(@"iPhone  6 Plus");
				_deviceHeight = 736;
				_tutorialSizeModifier = 52;
				break;
			default:
				break;
		}
		
	} else {
		// Do Portrait iPad things.
		
	}
		
	//} // END portrait orientation
}





- (void)customizeInterface {
	

	
	UIPageControl *pageControl = [UIPageControl appearance];
	pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
	pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
//	pageControl.backgroundColor = [UIColor redColor];

	
	
	
	_window.backgroundColor = [UIColor whiteColor];
	
	
	
	
	// Navigation & Status Bar Appearance
//	[[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor redColor]];

	[[UINavigationBar appearance] setTintColor:[Colors get].gray555];
	[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [Colors get].gray555}];
	//	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

	


	
	
	
	
	
	
	

	//### Tabbar Appearance ###//
	
	// Background Color
	[[UITabBar appearance] setBarTintColor: [Colors get].lightGray];
	
	
	
//	
//	//### Tabbar Icons ###//

	//## Normal / off state
	// Unselected icon tint color

	//	[[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[Colors get].darkOrange];
	

	// Text tint color for StateNormal..
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName: [Colors get].gray999}
										   forState:UIControlStateNormal];

	
	//## Selected State

	// Selected image color
	[[UITabBar appearance] setTintColor: [Colors get].gray333];

//
	// Text tint color for StateSelected..
	//[UITabBarItem.appearance setTitleTextAttributes:
	 //@{NSForegroundColorAttributeName: [UIColor greenColor]}
	//									   forState:UIControlStateSelected];
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName: [Colors get].gray333}
										   forState:UIControlStateSelected];

//	// Image tint color for StateSelected..
//	//[[UITabBar appearance] setSelectedImageTintColor:[UIColor greenColor]];
//
//	
	
	
	
	
	
	

	
	[self defineHeightWidth];
	[self defineDeviceSize];
	
	if (IS_IPHONE_5) {
		_boolIsIphone5 = YES;
		NSLog(@"this is an iPhone 5... use the larger storyboard.");
	} else {
		_boolIsIphone5 = NO;
		NSLog(@"older iPhone... use the smally.");
	}
	
	NSLog(@"_deviceHeight: %f",_deviceHeight);
	NSLog(@"_tutorialSizeModifier: %f",_tutorialSizeModifier);
	
//	UIPageControl *pageControl = [UIPageControl appearance];
//	pageControl.pageIndicatorTintColor = [Colors get].medGray;
//	pageControl.currentPageIndicatorTintColor = [Colors get].darkGray;
//	pageControl.backgroundColor = [UIColor clearColor];
//	
//	
//	UIView *bgView = [[UIView alloc]initWithFrame:_window.frame];
//	
//	[_window addSubview:bgView];
}






							
- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

	NSLog(@"\n");
//	if (_homeScreenRestart == YES) {
//		
//		[_gameViewController cancelAlert:nil];
//		
//		//[self.gameViewController cancelAlert:nil];
//		//_homeScreenRestart = NO;
//	}
	
	
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	
//	NSLog(@"\n");
//
//	if (_homeScreenRestart == YES) {
//		
//		[_gameViewController cancelAlert:nil];
//		
//		// Reset the BOOL
//		_homeScreenRestart = NO;
//	}
	
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}








#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
	// The directory the application uses to store the Core Data store file. This code uses a directory named "com.defaultmethod.test" in the application's documents directory.
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
	// The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Score500" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
	// The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	// Create the coordinator and store
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Score500.sqlite"];
	NSError *error = nil;
	NSString *failureReason = @"There was an error creating or loading the application's saved data.";
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
		NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Score500" ofType:@"sqlite"]];
		NSError* err = nil;
		
		if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
			NSLog(@"Oops, could copy preloaded data");
		}
	}
	
	// Handle the DB upgrade
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
							 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	
	
	
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
		
		// Report any error we got.
		NSMutableDictionary *dict = [NSMutableDictionary dictionary];
		dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
		dict[NSLocalizedFailureReasonErrorKey] = failureReason;
		dict[NSUnderlyingErrorKey] = error;
		error = [NSError errorWithDomain:@"com.defaultmethod.Score500" code:9999 userInfo:dict];
		// Replace this with code to handle the error appropriately.
		// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
	// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (!coordinator) {
		return nil;
	}
	_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		NSError *error = nil;
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}


@end
