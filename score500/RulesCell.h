//
//  RulesCell.h
//  Score500
//
//  Created by Drew Bombard on 6/11/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RulesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgTrump;
@property (strong, nonatomic) IBOutlet UILabel *lblTricks7;
@property (strong, nonatomic) IBOutlet UILabel *lblTricks8;
@property (strong, nonatomic) IBOutlet UILabel *lblTricks9;
@property (strong, nonatomic) IBOutlet UILabel *lblTricks10;


@end
