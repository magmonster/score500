//
//  UIView+Screenshot.m
//  Score500
//
//  Created by Drew Bombard on 4/4/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import "UIView+Screenshot.h"

@implementation UIView (Screenshot)

-(UIImage *)convertViewToImage {
	
	UIGraphicsBeginImageContext(self.bounds.size);
	
	[self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

@end
