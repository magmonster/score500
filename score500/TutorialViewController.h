//
//  RootViewController.h
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIPageViewControllerDataSource>

#pragma mark - Public Methods
- (void)goToPreviousContentViewController;
- (void)goToNextContentViewController;


@property (strong, nonatomic) NSArray *contentPageRestorationIDs;
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
