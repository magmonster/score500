//
//  Constants.m
//  score500
//
//  Created by Drew Bombard on 4/25/14.
//  Copyright (c) 2016 default_method. All rights reserved.
//

#import "Constants.h"

@implementation Constants


-(id)init {
	self = [super init];
	if (self) {
	
		
		
		_sampleAdUnitID = @"ca-app-pub-7096232266642103/6237860879";
		
	}
	return self;
}

+(Constants *)get {
	static Constants* constant = nil;
	if (!constant) {
		constant = [[Constants alloc] init];
	}
	return constant;
}


@end
