//
//  AppStats.m
//  Frolfer
//
//  Created by Drew Bombard on 2/25/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "AppStats.h"

@implementation AppStats

+(NSString *) appVersion {
	return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+(NSString *) build {
	return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+(NSString *) versionAndBuild {

	NSString * version = [self appVersion];
	NSString * build = [self build];
	
	NSString * versionBuild = [NSString stringWithFormat: @"v.%@", version];
	
	if (![version isEqualToString: build]) {
		versionBuild = [NSString stringWithFormat: @"%@.%@", versionBuild, build];
	}
	
	return versionBuild;
}

+(NSString *) version {
	NSString * version = [NSString stringWithFormat: @"v.%@", [self appVersion]];
	return version;
}

@end
