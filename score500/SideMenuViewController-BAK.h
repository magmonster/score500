//
//  SideMenuViewController.h
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

#import "GameViewController.h"
#import "SetupViewController.h"
#import "RulesViewController.h"
#import "InfoViewController.h"

#import "UIViewController+REFrostedViewController.h"
#import "MainNavigationController.h"

@interface SideMenuViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UIView *menubackviewTEST;

@end
