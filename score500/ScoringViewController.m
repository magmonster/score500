//
//  ScoringViewController.m
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "ScoringViewController.h"

@interface ScoringViewController ()

@end

@implementation ScoringViewController



@synthesize keyboardControls;

// These parameters handle the scrolling when tabbing through input fields
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


static const CGFloat DEFAULT_BID_Y_POS_A = 80;
static const CGFloat DEFAULT_BID_Y_POS_B = 80;

static const CGFloat DEFAULT_BID_X_POS_A = 50;
static const CGFloat DEFAULT_BID_X_POS_B = 269;



- (void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self setupKeyboardControls];
	
	[self customizeInterface];
	

	/* Debug Data */
	NSLog(@"Scoring: viewDidLoad()");
	NSLog(@"_bidTrump: %@",_bidTrump);
	NSLog(@"_bidNumber: %@",_bidNumber);
	NSLog(@"_biddingTeam: %@",_bidTeam);
	
	NSLog(@"_teamNameA: %@",_teamNameA);
	NSLog(@"_teamNameB: %@",_teamNameB);
	
	NSLog(@"scoreType: %@",_scoreType);
	//*/

	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;

	
	
	[self setupBiddingData];

	
	
	// Show the Ad Banners if this is the Free/Lite version
	if (appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	

	
	
	if ([_scoreType  isEqual: @"edit"]) {
				
		[self enableSaveButton];
		
		NSLog(@"_selected_hand: %@", _selected_hand);
		NSLog(@"\n");
		
		_bidTrump = _selected_hand.bidSuit;
		_bidNumber = [_selected_hand.bidCount stringValue];
		_bidTeam = _selected_hand.biddingTeam;
		
		_lblTeamScoreA.text = [_selected_hand.scoreTeamA stringValue];
		_lblTeamScoreB.text = [_selected_hand.scoreTeamB stringValue];
		_txtTeamTricksA.text = [_selected_hand.tricksTeamA stringValue];
		_txtTeamTricksB.text = [_selected_hand.tricksTeamB stringValue];
		
		_biddingTeamName = _selected_hand.bidWinner;
		
	} else {
		
		_bidTrump = _bidding_info.bidSuit;
		_bidNumber = [_bidding_info.bidNum stringValue];
		_bidTeam = _bidding_info.bidTeam;
		_biddingTeamName = _bidding_info.bidTeamName;
		
		_teamAPoints = 0;
		_teamBPoints = 0;
		
		[self disableSaveButton];
	}

	
	
	
	[self setupTrumpData];
	
	

	
	
	if ([_scoreType  isEqual: @"edit"]) {
		[self calculateScore];
	}
		
	/*/ Debug Data /*/
	NSLog(@"_bidTrump: %@",_bidTrump);
	NSLog(@"_bidTrump: %@",_bidNumber);
	NSLog(@"_editRowIndex: %d",_editRowIndex);
	NSLog(@"incomingScoreA: %@", _editScoreA);
	NSLog(@"incomingScoreB: %@", _editScoreB);
	NSLog(@"\n");
}

-(void)viewDidUnload {
	
	[super viewDidUnload];
	
	[self setTxtTeamTricksA:nil];
	[self setTxtTeamTricksB:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
	
	
	NSLog(@"scoreType: %@",_scoreType);
	
	[self setupTeamNamesGameType];
	
	
	if ([_bidTeam isEqual: @"teamA"]) {
		_biddingTeamName = _teamNameA;
	} else if ([_bidTeam isEqual: @"teamB"]) {
		_biddingTeamName = _teamNameB;
	}
	
	[self setupViewDataText];
}



-(void)customizeInterface {
	
	
	// Apple a border 1px thick
	//	_view_backPanel.layer.borderColor = [UIColor redColor].CGColor;
	// /*[UIColor colorWithWhite:1.0f alpha:1.0f].CGColor; */
	//view_backPanel.layer.borderWidth = 1.0f;
	
	_borderTeam1Tricks.layer.borderWidth = 0.5;
	_borderTeam1Tricks.layer.borderColor = [[[Colors get] gray999] CGColor];
	_borderTeam2Tricks.layer.borderWidth = 0.5;
	_borderTeam2Tricks.layer.borderColor = [[[Colors get] gray999] CGColor];
	
	
	_btnSaveScore.backgroundColor = [Colors get].medGreen;
	_btnSaveScore.titleLabel.textColor = [Colors get].gray666;
	
	_btnSaveScore.layer.cornerRadius = _btnSaveScore.frame.size.height/2;
	_btnSaveScore.layer.masksToBounds = YES;
}






-(void)setupTrumpData {
	
	
	NSError *error;
	NSMutableArray *dataArr;
	
	// 1 - Decide what Entity you want
	NSLog(@"Fetching data for the entity named -----> %@", @"Trump");
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	//	// Sort by bidSuit
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"trumpSuit = %@ AND trickCount = %@",_bidTrump,_bidNumber];
	
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Trump" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	dataArr = [[NSMutableArray alloc]initWithArray:fetchedObjects];
	
	
	
	
	_trumpData = [[NSMutableArray alloc]initWithArray:fetchedObjects];
	_trump_info = [_trumpData objectAtIndex:0];
	
	
	NSLog(@"_trumpData: %@",_trumpData);
	NSLog(@"_trump_info: %@",_trump_info);
	NSLog(@"_trump_info.trickCount: %@",_trump_info.trickCount);
	NSLog(@"_trump_info.trumpValue: %@",_trump_info.trumpValue);
	NSLog(@"_trump_info.trumpSuit: %@",_trump_info.trumpSuit);
	NSLog(@"\n");
}



#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		// Move the start/stop button up...
		//		float xPos = _btns_start_stop.center.x;
		//		float yPos = _btns_start_stop.center.y;
		
		//		float xPosSettings = _btn_settings.center.x;
		//		float yPosSettings = _btn_settings.center.y;
		
		//		yPos = yPos - 50;
		//		yPosSettings = yPosSettings - 10;
		
		
		
		// Move the view up
		[UIView animateWithDuration:0.2
						 animations:^{
							 
							 _contentView.frame = CGRectMake(
															 _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height - _bannerView.frame.size.height));
							 
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the start/stop button back down...
		//		float xPos = _btns_start_stop.center.x;
		//		float yPos = _btns_start_stop.center.y;
		
		//		yPos = yPos + 50;
		
		//		float xPosSettings = _btns_start_stop.center.x;
		//		float yPosSettings = _btn_settings.center.y;
		
		//		yPosSettings = yPosSettings + 10;
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
							 
							 _contentView.frame = CGRectMake(
															 _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height + _bannerView.frame.size.height));
							 
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}









-(IBAction)editingDidBegin:(id)sender {
	
	NSLog(@"\n");
	NSLog(@"editingDidBegin()");
	
	// Let's clear out the int values...
	_teamAPoints = 0;
	_teamBPoints = 0;
	
	_txtTeamTricksA.text = NULL;
	_txtTeamTricksB.text = NULL;
}


-(void)enableSaveButton {
	_btnSaveScore.alpha = 1.0;
	_btnSaveScore.enabled = YES;
}

-(void)disableSaveButton {
	_btnSaveScore.alpha = 0.5;
	_btnSaveScore.enabled = NO;
}

-(void)enableBidMessage:(NSString *)theMessage {
	
	_lblStatusMessage.text = theMessage;
}

-(void)resetBidData {
	
	_lblStatusMessage.text = NULL;
	
	_txtTeamTricksA.placeholder = @"- -";
	_txtTeamTricksB.placeholder = @"- -";
	
	_lblTeamScoreA.text = @"- -";
	_lblTeamScoreB.text = @"- -";
	
	_imgBidIndicatorA.image = NULL;
	_imgBidIndicatorB.image = NULL;
}




-(void)calculateBelowStandardBid:(NSString *)biddingTeam
						 imgBidA:(UIImage *)imgBidA
						 imgBidB:(UIImage *)imgBidB {
	
	NSLog(@"Just spit out some scores... bid was lower than standard");
	
	/*
	 * Bidding team has entered a bid below a standard 500 bid (7 - 10)
	 */
	_teamAPoints = [_txtTeamTricksA.text intValue] * 10;
	_teamBPoints = [_txtTeamTricksB.text intValue] * 10;
}

-(void)setTeamScores:(NSString *)biddingTeam
			 imgBidA:(UIImage *)imgBidA
			 imgBidB:(UIImage *)imgBidB
			 aPoints:(int)aPoints
			 bPoints:(int)bPoints {
	
	_imgBidIndicatorA.image = imgBidA;
	_imgBidIndicatorB.image = imgBidB;
	
	
	_teamAPoints = aPoints;
	_teamBPoints = bPoints;
	
	NSLog(@"imgBidA: %@",imgBidA);
	NSLog(@"imgBidB: %@",imgBidB);
	
	
	
	NSLog(@"teamAPoints: %d",_teamAPoints);
	NSLog(@"teamAPoints: %d",_teamBPoints);
	
	
	// Send scores to the screen
	_lblTeamScoreA.text = [NSString stringWithFormat:@"%d", _teamAPoints];
	_lblTeamScoreB.text = [NSString stringWithFormat:@"%d", _teamBPoints];
}

-(void)loadScores:(NSString *)scoreTeamA
	   scoreTeamB:(NSString *)scoreTeamB {
	
	[self setTeamScores:_bidTeam
				imgBidA:nil
				imgBidB:nil
				aPoints:[_editScoreA intValue]
				bPoints:[_editScoreB intValue]];
}


-(void)calculateScore {
	
	NSLog(@"\n");
	NSLog(@"checkScoring()");
	
	UIImage *imgBiddingA;
	UIImage *imgBiddingB;
	int pointsTeamA = 0;
	int pointsTeamB = 0;
	
	
	
	int bidValue = [_trump_info.trumpValue intValue];
	
	NSLog(@"_bidNumber: %@",_bidNumber);
	NSLog(@"_trump_info: %@",_trump_info);
	NSLog(@"bidScore: %d",bidValue);
	NSLog(@"\n");
	
	
	/**
	 * Fill in the opposite the opponent's trick count
	 */
	if([_txtTeamTricksA.text length] > 0) {
		_txtTeamTricksB.text = [NSString stringWithFormat:@"%d",10 - [_txtTeamTricksA.text intValue]];
	} else if ([_txtTeamTricksB.text length] > 0) {
		_txtTeamTricksA.text = [NSString stringWithFormat:@"%d",10 - [_txtTeamTricksB.text intValue]];
	}
	
	
	
	
	
	
	
	
	
	
	
	
	if ([_bidTeam isEqual: @"teamA"]) {
		
		/**
		 * Bidding Team entered a bid below the standard (6 or less)
		 */
		if ([_bidNumber intValue] < 6) {
			
			if ( ([_txtTeamTricksA.text length] > 0) && [_txtTeamTricksA.text intValue] >= [_bidNumber intValue]) {
				boolBiddingTeamSet = NO;
				imgBiddingA = [UIImage imageNamed:@"check"];
				imgBiddingB = [UIImage imageNamed:@"check_x"];
				_winningTeam = @"teamA";
				
			} else {
				boolBiddingTeamSet = YES;
				imgBiddingA = [UIImage imageNamed:@"check_x"];
				imgBiddingB = [UIImage imageNamed:@"check"];
				_winningTeam = @"teamB";
			}
			
			
			
			pointsTeamA = [_txtTeamTricksA.text intValue]*10;
			pointsTeamB = [_txtTeamTricksB.text intValue]*10;
			
			
			
		} else {
			
			
			if ( ([_txtTeamTricksA.text length] > 0) && [_txtTeamTricksA.text intValue] == 10) {
				
				boolBiddingTeamSet = NO;
				pointsTeamA = 250;
				pointsTeamB = 0;
				
				imgBiddingA = [UIImage imageNamed:@"check"];
				imgBiddingB = [UIImage imageNamed:@"check_x"];
				_winningTeam = @"teamA";
				
			}
			
			
			else if ( ([_txtTeamTricksA.text length] > 0) && [_txtTeamTricksA.text intValue] >= [_bidNumber intValue]) {
				
				NSLog(@"\n\nteamA has made their bid!\n\n");
				
				/**
				 * Bidding team has made the bid.
				 * Now let's grab their score...
				 */
				boolBiddingTeamSet = NO;
				pointsTeamA = bidValue;
				pointsTeamB = [_txtTeamTricksB.text intValue]*10;
				
				imgBiddingA = [UIImage imageNamed:@"check"];
				imgBiddingB = [UIImage imageNamed:@"check_x"];
				_winningTeam = @"teamA";
				
			} else {
				
				/**
				 * Bidding team has BEEN SET.
				 * Apply a negative value to their score and add
				 * up the tricks for team B.
				 */
				NSLog(@"\n\nteamA has been set... SUCKA!\n\n");
				
				boolBiddingTeamSet = YES;
				bidValue = -bidValue;
				pointsTeamA = bidValue;
				pointsTeamB = [_txtTeamTricksB.text intValue]*10;
				
				imgBiddingA = [UIImage imageNamed:@"check_x"];
				imgBiddingB = [UIImage imageNamed:@"check"];
				_winningTeam = @"teamB";
				
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}
	else if ([_bidTeam isEqual: @"teamB"]) {
		
		/**
		 * Bidding Team entered a bid below the standard (6 or less)
		 */
		if ([_bidNumber intValue] < 6) {
			
			if ( ([_txtTeamTricksA.text length] > 0) && [_txtTeamTricksA.text intValue] >= [_bidNumber intValue]) {
				boolBiddingTeamSet = NO;
				imgBiddingA = [UIImage imageNamed:@"check_x"];
				imgBiddingB = [UIImage imageNamed:@"check"];
				_winningTeam = @"teamB";
				
			} else {
				boolBiddingTeamSet = YES;
				imgBiddingA = [UIImage imageNamed:@"check"];
				imgBiddingB = [UIImage imageNamed:@"check_x"];
				_winningTeam = @"teamA";
			}
			
			pointsTeamA = [_txtTeamTricksA.text intValue]*10;
			pointsTeamB = [_txtTeamTricksB.text intValue]*10;
			
			
		} else {
			
			if ( ([_txtTeamTricksB.text length] > 0) && [_txtTeamTricksB.text intValue] == 10) {
				
				boolBiddingTeamSet = NO;
				pointsTeamA = 0;
				pointsTeamB = 250;
				
				imgBiddingA = [UIImage imageNamed:@"check_x"];
				imgBiddingB = [UIImage imageNamed:@"check"];
				_winningTeam = @"teamB";
			}
			
			else if ( ([_txtTeamTricksB.text length] > 0) && [_txtTeamTricksB.text intValue] >= [_bidNumber intValue]) {
				
				/**
				 * Bidding team has made the bid.
				 * Now let's grab their score...
				 */
				boolBiddingTeamSet = NO;
				pointsTeamA = [_txtTeamTricksA.text intValue]*10;
				pointsTeamB = bidValue;
				
				imgBiddingA = [UIImage imageNamed:@"check_x"];
				imgBiddingB = [UIImage imageNamed:@"check"];
				_winningTeam = @"teamB";
				
			} else {
				
				/**
				 * Bidding team has BEEN SET.
				 * Apply a negative value to their score and add
				 * up the tricks for team A.
				 */
				NSLog(@"\n\nteamB has been set... SUCKA!\n\n");
				
				boolBiddingTeamSet = YES;
				bidValue = -bidValue;
				pointsTeamA = [_txtTeamTricksA.text intValue]*10;
				pointsTeamB = bidValue;
				
				imgBiddingA = [UIImage imageNamed:@"check"];
				imgBiddingB = [UIImage imageNamed:@"check_x"];
				_winningTeam = @"teamA";
				
			}
		}
	}
	
	
	NSLog(@"winning team: %@",_winningTeam);
	
	[self setTeamScores: _bidTeam
				imgBidA: imgBiddingA
				imgBidB: imgBiddingB
				aPoints: pointsTeamA
				bPoints: pointsTeamB];
	
	if (boolBiddingTeamSet == YES) {
		[self enableBidMessage: [_biddingTeamName stringByAppendingString:@" was set!"]];
	} else if (boolBiddingTeamSet == NO) {
		[self enableBidMessage: [_biddingTeamName stringByAppendingString:@" met their bid."]];
	}
	
	
	
	NSLog(@"pointsTeamA: %d",pointsTeamA);
	
	
	
	[self enableSaveButton];
}


-(void)setupTeamNamesGameType {
	
	if (_teamNameA == nil) {
		_teamNameA = [[_teamData valueForKey:@"teamName"] objectAtIndex:0];
		_teamNameB = [[_teamData valueForKey:@"teamName"] objectAtIndex:1];
	}
	
	
	_lblTeamA.text = _teamNameA;
	_lblTeamB.text = _teamNameB;
}



-(float)findXPos:(NSString *)teamPosition {
	
	float xPos;
	if ([teamPosition isEqual: @"teamA"]) {
		xPos = DEFAULT_BID_X_POS_A;
	} else {
		xPos = DEFAULT_BID_X_POS_B;
	}
	return xPos;
}

-(float)findYPos:(NSString *)teamPosition {
	
	float yPos;
	if ([teamPosition isEqual: @"teamA"]) {
		yPos = DEFAULT_BID_Y_POS_A;
	} else {
		yPos = DEFAULT_BID_Y_POS_B;
	}
	return yPos;
}






-(void)setupBiddingData {
	
	if ([CoreDataCheck checkEntity:@"Bidding"] > 0) {
		[self fetchBiddingResults];
		_bidding_info = [_biddingData objectAtIndex:0];
		
		_bidTeam = _bidding_info.bidTeam;
		_biddingTeamName = _bidding_info.bidTeamName;
	}
	
	
}


#pragma mark - setup fetched data
-(void)fetchSetupResults {
	_teamData = [FetchDataArray dataFromEntity:@"Team" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	_setupData = [FetchDataArray dataFromEntity:@"Setup" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}

-(void)fetchBiddingResults {
	_biddingData = [FetchDataArray dataFromEntity:@"Bidding" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}








-(void)setupViewDataText {
	
	_lblTheBid.text = [[ @"Current Bid: " stringByAppendingString:_bidNumber] stringByAppendingString:[ @" " stringByAppendingString:_bidTrump]];
	_lblBiddingTeam.text = [@"Bidding Team: " stringByAppendingString:_biddingTeamName];

	
}


-(IBAction)saveScore:(id)sender {
	
	if ([_scoreType  isEqual: @"edit"]) {
		_selected_hand.bidWinner = _winningTeam;
		
		_selected_hand.tricksTeamA = [NSNumber numberWithInt: [_txtTeamTricksA.text intValue]];
		_selected_hand.tricksTeamB = [NSNumber numberWithInt: [_txtTeamTricksB.text intValue]];
		_selected_hand.scoreTeamA = [NSNumber numberWithInt: [_lblTeamScoreA.text intValue]];
		_selected_hand.scoreTeamB = [NSNumber numberWithInt: [_lblTeamScoreB.text intValue]];
		
		[self.managedObjectContext save:nil];  // write to database
	}
	
	
	if ([_scoreType isEqual: @"new"]) {
		
		NSError *error = nil;
		Hand *saveHandDetails = [NSEntityDescription
						insertNewObjectForEntityForName:@"Hand"
						inManagedObjectContext:_managedObjectContext];
		
		saveHandDetails.bidSuit = _bidding_info.bidSuit;
		saveHandDetails.bidCount = _bidding_info.bidNum;
		saveHandDetails.biddingTeam = _bidding_info.bidTeam;
		
		saveHandDetails.bidWinner = _bidding_info.bidTeamName;
		saveHandDetails.tricksTeamA = [NSNumber numberWithInt: [_txtTeamTricksA.text intValue]];
		saveHandDetails.tricksTeamB = [NSNumber numberWithInt: [_txtTeamTricksB.text intValue]];
		saveHandDetails.scoreTeamA = [NSNumber numberWithInt: [_lblTeamScoreA.text intValue]];
		saveHandDetails.scoreTeamB = [NSNumber numberWithInt: [_lblTeamScoreB.text intValue]];
		saveHandDetails.sortOrder = [NSDate date];
		
		[_managedObjectContext save:&error];
		
		[self fetchBiddingResults];
	}
	
	
	[self dismissModal:nil];
}

-(IBAction)dismissModal:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}



-(void)alertScoreTooHigh
{
	NSString *alertMessage = @"Trick count set too high.";
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Alert"
								 message:alertMessage
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* okButton = [UIAlertAction
							   actionWithTitle:@"OK"
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction * action) {
								   NSLog(@"OK button clicked");
								   
								   [self resetBidData];
							   }];
	
	[alert addAction:okButton];
	[self presentViewController:alert animated:YES completion:nil];
}
















#pragma mark - Keyboard controls

/* Setup the keyboard controls */
-(void)setupKeyboardControls {
	
	// Initialize the keyboard controls
	self.keyboardControls = [[BSKeyboardControls alloc] init];

	// Set the delegate of the keyboard controls
	self.keyboardControls.delegate = self;


	// Add all text fields you want to be able to skip between to the keyboard controls
	// The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
	NSArray *fields = @[ self.txtTeamTricksA,
						 self.txtTeamTricksB];

	[self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
	[self.keyboardControls setDelegate:self];






	// Set the style of the bar. Default is UIBarStyleBlackTranslucent.
	//self.keyboardControls.barStyle = UIBarStyleBlackTranslucent;


	// Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
	//self.keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];

	// Set title for the "Previous" button. Default is "Previous".
	self.keyboardControls.previousTitle = @"Previous";

	// Set title for the "Next button". Default is "Next".
	self.keyboardControls.nextTitle = @"Next";

	// Add the keyboard control as accessory view for all of the text fields
	// Also set the delegate of all the text fields to self
	
	
}

/* Scroll the view to the active text field */
-(void)scrollViewToTextField:(id)textField {
    UITableViewCell *cell = nil;
    if ([textField isKindOfClass:[UITextField class]])
        cell = (UITableViewCell *) ((UITextField *) textField).superview.superview;
    else if ([textField isKindOfClass:[UITextView class]])
        cell = (UITableViewCell *) ((UITextView *) textField).superview.superview;
	//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


/* Either "Previous" or "Next" was pressed
 * Here we usually want to scroll the view to the active text field
 * If we want to know which of the two was pressed, we can use the "direction" which will have one of the following values:
 * KeyboardControlsDirectionPrevious "Previous" was pressed
 * KeyboardControlsDirectionNext "Next" was pressed
 */
//-(void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField {
//    [textField becomeFirstResponder];
//    [self scrollViewToTextField:textField];
//}


#pragma mark -
#pragma mark Text Field Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
	
	
	[self resetBidData];
	
	
	[self.keyboardControls setActiveField:textField];

	if ([self.keyboardControls.fields containsObject:textField])
		self.keyboardControls.activeField = textField;
	[self scrollViewToTextField:textField];
	
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	
	if (heightFraction < 0.0) {
		heightFraction = 0.0;
	} else if (heightFraction > 1.0) {
		heightFraction = 1.0;
	}
	
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
		animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	} else {
		animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
	
	
}

#pragma mark -
#pragma mark Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	[self.keyboardControls setActiveField:textView];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
	UIView *view;
	view = field.superview.superview.superview;
}

/*
 * The "Done" button was pressed
 * We want to close the keyboard
 */
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
	[self.view endEditing:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField  {
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(IBAction)dismissKeyboard:(id)sender {
	
	[self.txtTeamTricksA resignFirstResponder];
	[self.txtTeamTricksB resignFirstResponder];
	
	if ( ([_txtTeamTricksA.text intValue] >= 11) || ([_txtTeamTricksB.text intValue] >= 11) )  {
		
		NSLog(@"Alert: User has entered a score too high...");
		[self alertScoreTooHigh];
		
		[self resetBidData];
	} else {
		
		// Only check scoring if they entered a score.
		if ( [_txtTeamTricksA.text length] > 0 || [_txtTeamTricksB.text length] > 0 ) {
			[self calculateScore];
		} else {
			[self enableBidMessage:@"Enter tricks taken"];
		}
	}
}



@end
