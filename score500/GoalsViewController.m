//
//  GoalsViewController.m
//  Score500
//
//  Created by Drew Bombard on 5/24/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "GoalsViewController.h"

NSString * const BannerViewActionWillBegin = @"BannerViewActionWillBegin";
NSString * const BannerViewActionDidFinish = @"BannerViewActionDidFinish";

@interface GoalsViewController ()

@end


@implementation GoalsViewController





#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;

	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		// Move the start/stop button up...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		float xPosSettings = _btn_settings.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPos = yPos - 50;
//		yPosSettings = yPosSettings - 10;
		
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the start/stop button back down...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		yPos = yPos + 50;
		
//		float xPosSettings = _btns_start_stop.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPosSettings = yPosSettings + 10;
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}














-(void)customizeInterface {
	_btnTutorial.backgroundColor = [Colors get].darkOrange;
	_btnTutorial.titleLabel.textColor = [UIColor whiteColor];
}

-(void)viewDidLoad {

	[super viewDidLoad];
	// Do any additional setup after loading the view.

	
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	// Show the Ad Banners if this is the Free/Lite version
	if (_appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];

}


@end
