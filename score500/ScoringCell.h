//
//  ScoringCell.h
//  score500
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoringCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblScore;

@property (strong, nonatomic) IBOutlet UILabel *lblLineNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblScoreA;
@property (strong, nonatomic) IBOutlet UILabel *lblScoreB;


@end
