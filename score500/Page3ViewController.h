//
//  Page3ViewController.h
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseContentViewController.h"

@interface Page3ViewController : BaseContentViewController

#pragma mark - Actions
- (IBAction)page2ButtonTapped:(UIBarButtonItem *)sender;

@end
