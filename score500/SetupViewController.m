//
//  SetupViewController.m
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@end

@implementation SetupViewController


@synthesize keyboardControls;
// These parameters handle the scrolling when tabbing through input fields
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;








//-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
//	
//	NSLog(@"\n");
//	NSLog(@"Notice: bannerview did not receive any banner due to %@", error);
//	
//	_banner.hidden = YES;
////	[self.bannerView removeFromSuperview];
//}
//
//-(void)bannerViewActionDidFinish:(ADBannerView *)banner{
//	NSLog(@"\n");
//	NSLog(@"bannerview was selected");
//}
//
//- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{
//	NSLog(@"\n");
//	return YES;
//}
//
//-(void)bannerViewDidLoadAd:(ADBannerView *)banner {
//	NSLog(@"\n");
//	NSLog(@"banner was loaded");
//	_banner.hidden = NO;
//}











-(IBAction)segmentValueChanged:(UISegmentedControl *)sender {

	switch (sender.selectedSegmentIndex) {
		case 0:
			NSLog(@"Play to Rounds");
			[self enablePlayToRounds];
			
			break;
		case 1:
			NSLog(@"Play to 500");
			[self enablePlayTo500];
			
			break;
		default:
			break;
	}
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	NSLog(@"prepareForSegue()");
	
	if ([segue.identifier isEqualToString:@"beginSegue"]) {
		NSLog(@"Let's move, we are in the segue....");
	} else {
		NSLog(@"Unidentified Segue Attempted!");
	}
	
}


-(void)setupViewContent {
	
	/**
	 * Send data out of the view..
	 */
	if ( [_teamNameA length] != 0) {
		_txtTeamNameA.text = _teamNameA;
	}
	if ( [_teamNameB length] != 0) {
		_txtTeamNameB.text = _teamNameB;
	}
	_txtGameRounds.text = [_numRounds stringValue];
	
	
	if ([_gameType isEqual: @"500"]) {
		[self enablePlayTo500];
	} else {
		[self enablePlayToRounds];
	}
}



-(void)viewWillAppear:(BOOL)animated {
	
	NSLog(@"viewWillAppear");
	

	
	_btnBeginGame.enabled = false;
	
	
	/**
	 * Check for local data.  If there is some,
	 * (and there should be) then grab the data.
	 */
	if ([CoreDataCheck checkEntity:@"Setup"] > 0) {
		[self fetchSetupResults];
		_setup_info = [_setupData objectAtIndex:0];
		
		_team_info_A = [_teamData objectAtIndex:0];
		_team_info_B = [_teamData objectAtIndex:1];
	}
	

	NSLog(@"\n\n_team_info_A: %@", _team_info_A.teamName);
	NSLog(@"\n\n_team_info_B: %@", _team_info_B.teamName);

	NSLog(@"\n\n");
	NSLog(@"_teamData: %@", _teamData);
	NSLog(@"\n\n");
	NSLog(@"_teamData objectAtIndex: %@", [_teamData objectAtIndex:0]);
	NSLog(@"\n\n");
	
	
	/**
	 * Set local page variables
	 */
	_gameType = _setup_info.gameType;
	_numRounds = _setup_info.numRounds;
	_teamNameA = _team_info_A.teamName;
	_teamNameB = _team_info_B.teamName;
	
	if ([_setup_info.gameType  isEqual: @"rounds"]){
		_segmentedControl.selectedSegmentIndex = 0;
	}

	NSLog(@"_teamNameA: %@", _teamNameA);
	NSLog(@"_teamNameB: %@", _teamNameB);
	NSLog(@"gameType: %@", _setup_info.gameType);
	NSLog(@"numRounds: %@", _setup_info.numRounds);
	
	[self setupViewContent];
}



#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.

	
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		
		
		// Move the view up
		[UIView animateWithDuration:0.2
						 animations:^{
							 
							 _contentView.frame = CGRectMake(
															  _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height - _bannerView.frame.size.height));

						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
							 _contentView.frame = CGRectMake(
															  _contentView.frame.origin.x
															 ,_contentView.frame.origin.y
															 ,_contentView.frame.size.width
															 ,(_contentView.frame.size.height + _bannerView.frame.size.height));
						}];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}







-(void)customizeInterface {
	
	_btnBeginGame.backgroundColor = [Colors get].medGreen;
	_btnBeginGame.titleLabel.textColor = [UIColor whiteColor];

	[[UISegmentedControl appearance] setTintColor: [Colors get].darkGray];
	
	_borderTeam1.layer.borderWidth = 0.5;
	_borderTeam1.layer.borderColor = [[[Colors get] gray999] CGColor];
	_borderTeam2.layer.borderWidth = 0.5;
	_borderTeam2.layer.borderColor = [[[Colors get] gray999] CGColor];
	
	_btnBeginGame.layer.cornerRadius = 24;
	_btnBeginGame.layer.masksToBounds = YES;	
}

- (void)viewDidLoad {
	
	NSLog(@"viewDidLoad");
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	
	
	// Show the Ad Banners if this is the Free/Lite version
	if (appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];
	
}

- (void)dealloc {
	_bannerView.delegate = nil;
}

-(void)viewDidUnload {
	[super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}




#pragma mark - Game Types
-(void)enablePlayToRounds {
	
	[self disablePlayTo500];
	
	
	if ([_txtGameRounds.text length] > 0) {
		[self enableBeginGame];
	}

	
	_gameType = @"rounds";
	_numRounds = [NSNumber numberWithInt: [_txtGameRounds.text intValue]];
	_roundsMessageView.hidden = NO;
}

-(void)enablePlayTo500 {
		
	[self enableBeginGame];
	[self disablePlayToRounds];
	
// Do I need to still set this ?!?
	_gameType = @"500";
}

-(void)disablePlayToRounds {
	
	// Set text input to "off" state..
	_txtGameRounds.text = @"";
	_roundsMessageView.hidden = YES;
}

-(void)disablePlayTo500 {
	if ( [_gameType  isEqual: @"rounds"] && [_txtGameRounds.text isEqual: @""] ) {
		[self disableBeginGame];
	}
}

-(void)enableBeginGame {
	_btnBeginGame.enabled = true;
	_btnBeginGame.alpha = 1.0;
}

-(void)disableBeginGame {

	NSLog(@"\n\ndisableBeginGame()");

	_btnBeginGame.enabled = false;
	_btnBeginGame.alpha = 0.6;
}


-(IBAction)numrounds_started_editing:(id)sender {
	NSLog(@"numrounds_started_editing....");
	
	// Disable "play to 500" radio..
	[self disablePlayTo500];
	
	
	[self enablePlayToRounds];
}


#pragma mark - Core Data (Fetching/Editing/Saving)

-(void)fetchSetupResults {
	_teamData = [FetchDataArray dataFromEntity:@"Team" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	_setupData = [FetchDataArray dataFromEntity:@"Setup" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
}


-(IBAction)saveSetup:(id)sender {
	
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSNumber *numRounds;
	
	if ([_gameType  isEqual: @"500"]) {
		numRounds = [numberFormatter numberFromString:@"0"];
	} else {
		numRounds = [numberFormatter numberFromString: _txtGameRounds.text];
	}
	
	NSString *tmpTeamNameA;
	NSString *tmpTeamNameB;
	if (_txtTeamNameA.text.length == 0) {
		tmpTeamNameA = _txtTeamNameA.placeholder;
	} else {
		tmpTeamNameA = _txtTeamNameA.text;
	}
	if (_txtTeamNameB.text.length == 0) {
		tmpTeamNameB = _txtTeamNameB.placeholder;
	} else {
		tmpTeamNameB = _txtTeamNameB.text;
	}
	

	/**
	 * Reset the team name to text input value
	 */
	_team_info_A.teamName = tmpTeamNameA;
	_team_info_B.teamName = tmpTeamNameB;
		
	/**
	 * Reset setup data to segment value and
	 * (if applicable) the number of game rounds
	 */
	_setup_info.gameType = _gameType;
	_setup_info.numRounds = numRounds;
	
	
	[self saveSetupData];
//	[self performSegueWithIdentifier:@"beginSegue" sender:nil];
	
	self.tabBarController.selectedIndex = 0;
}

-(void)saveSetupData {
	
	NSError *error;
	_setup_info.gameType = _gameType;
	_setup_info.numRounds = _numRounds;
		
	[self.managedObjectContext save:&error];
	NSLog(@"\n\n");
}















#pragma mark - Keyboard controls

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	
	NSLog(@"blah....");
	return YES;
}


/* Setup the keyboard controls */
-(void)setupKeyboardControls {
	
    // Initialize the keyboard controls
    self.keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    self.keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
	NSArray *fields = @[ self.txtGameRounds,
						 self.txtTeamNameA,
						 self.txtTeamNameB];
	
	[self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
	[self.keyboardControls setDelegate:self];

	
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    //self.keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
	
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    self.keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    self.keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    self.keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
	
	
//	[self.keyboardControls reloadTextFields];
}



/* Scroll the view to the active text field */
-(void)scrollViewToTextField:(id)textField {
    UITableViewCell *cell = nil;
    if ([textField isKindOfClass:[UITextField class]])
        cell = (UITableViewCell *) ((UITextField *) textField).superview.superview;
    else if ([textField isKindOfClass:[UITextView class]])
        cell = (UITableViewCell *) ((UITextView *) textField).superview.superview;
	//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


/*
 * The "Done" button was pressed
 * We want to close the keyboard
 */
-(void)keyboardControlsDonePressed:(BSKeyboardControls *)controls {
//    [controls.activeTextField resignFirstResponder];
	[self.view endEditing:YES];

}
/* Either "Previous" or "Next" was pressed
 * Here we usually want to scroll the view to the active text field
 * If we want to know which of the two was pressed, we can use the "direction" which will have one of the following values:
 * KeyboardControlsDirectionPrevious "Previous" was pressed
 * KeyboardControlsDirectionNext "Next" was pressed
 */
//-(void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField {
//    [textField becomeFirstResponder];
//    [self scrollViewToTextField:textField];
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
	
	[self.keyboardControls setActiveField:textField];

	
//	if ([self.keyboardControls.textFields containsObject:textField])
//		self.keyboardControls.activeTextField = textField;
//	[self scrollViewToTextField:textField];
	
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;

	if (heightFraction < 0.0) {
		heightFraction = 0.0;
	} else if (heightFraction > 1.0) {
		heightFraction = 1.0;
	}
	
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
		animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	
	} else {
		animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
	
	
}

-(void)textFieldDidEndEditing:(UITextField *)textField  {
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(IBAction)dismissKeyboard:(id)sender {
	
	NSLog(@"\n\n");
	NSLog(@"dissmissKeyboard()");
	
	[self.txtGameRounds resignFirstResponder];
	[self.txtTeamNameA resignFirstResponder];
	[self.txtTeamNameB resignFirstResponder];
	
	
	
	
	NSLog(@"_gameType: %@", _gameType);
	NSLog(@"_txtGameRounds.text length: %lu",(unsigned long)[_txtGameRounds.text length]);
	NSLog(@"_txtGameRounds.text: %@",_txtGameRounds.text);
	NSLog(@"\n\n");
	

	if ( [_gameType  isEqual: @"500"] || ([_gameType  isEqual: @"rounds"] && [_txtGameRounds.text length] > 0) ) {
		NSLog(@"Enable the 'begin' button");
		NSLog(@"\n\n");

		_numRounds = [NSNumber numberWithInt: [_txtGameRounds.text intValue]];

		[self enableBeginGame];
	} else {
		NSLog(@"DISABLE the 'begin' button");
		NSLog(@"\n\n");
		[self disableBeginGame];
	}
}


@end
