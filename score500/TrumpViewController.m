//
//  TrumpViewController.m
//  score500
//
//  Created by Drew Bombard on 2/13/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "TrumpViewController.h"

@interface TrumpViewController ()

@end

@implementation TrumpViewController


@synthesize biddingTeam = _biddingTeam;



@synthesize keyboardControls;
// These parameters handle the scrolling when tabbing through input fields
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


@synthesize trumpSuit = _trumpSuit;
@synthesize trumpSuitID = _trumpSuitID;

@synthesize heartSelected = _heartSelected;
@synthesize spadeSelected = _spadeSelected;
@synthesize clubSelected = _clubSelected;
@synthesize diamondSelected = _diamondSelected;

@synthesize txtHandBid = _txtHandBid;
@synthesize handBid = _handBid;

@synthesize teamA = _teamA;
@synthesize teamB = _teamB;

@synthesize trumpScroller = _trumpScroller;


@synthesize btnBidTeamA = _btnBidTeamA;
@synthesize btnBidTeamB = _btnBidTeamB;


@synthesize imgPanelBackground = _imgPanelBackground;



-(IBAction)trumpSpade:(id)sender {
	_trumpSuitID = 1;
	_trumpSuit = @"Spade";
	[self highlightTrump];
//	[self saveTrump:NULL];
}
-(IBAction)trumpClub:(id)sender {
	_trumpSuitID = 2;
	_trumpSuit = @"Club";
	[self highlightTrump];
//	[self saveTrump:NULL];
}
-(IBAction)trumpDiamond:(id)sender {
	_trumpSuitID = 3;
	_trumpSuit = @"Diamond";
	[self highlightTrump];
//	[self saveTrump:NULL];
}
-(IBAction)trumpHeart:(id)sender {
	_trumpSuitID = 4;
	_trumpSuit = @"Heart";
	[self highlightTrump];
//	[self saveTrump:NULL];
}
-(IBAction)trumpNo:(id)sender {
	_trumpSuitID = 5;
	_trumpSuit = @"No Trump";
	[self highlightTrump];
//	[self saveTrump:NULL];
}


-(IBAction)cancelTrumpSelection:(id)sender {

//	[self dismissViewControllerAnimated:YES completion:nil];
//	[self.delegate trumpSelectButtonWasTapped:self];

	[self saveTrump:nil];
}

-(void)saveTrump:(id)sender {
	

	NSLog(@"_trumpSuit: %d",_trumpSuitID);
	NSLog(@"_trumpSuit: %@",_trumpSuit);
	NSLog(@"biddingTeam: %@",_txtHandBid.text);
	NSLog(@"biddingTeam: %@",_biddingTeam);
	
	[self.delegate trumpSelectButtonWasTapped:self];

}


-(void)enableDisableTrump:(UIButton *)trump {


	
	for (int i = 0; i < _btnArr.count; i++) {
		if ([_btnArr objectAtIndex:i] != trump) {
			
			NSLog(@"Disable everthing else...");
			[[_btnArr objectAtIndex:i] setImage:[UIImage imageNamed: [_btnImgOFFArr objectAtIndex:i]] forState:UIControlStateNormal];
		} else {
			// Do NOTHING....
			NSLog(@"DO NOTHING .....");
			[[_btnArr objectAtIndex:i] setImage:[UIImage imageNamed: [_btnImgONArr objectAtIndex:i]] forState:UIControlStateNormal];
		}
	}
}

-(void)enableAllTrump {
	
	for (int i = 0; i < _btnArr.count; i++) {
			
		NSLog(@"Disable everthing else...");
		[[_btnArr objectAtIndex:i] setImage:[UIImage imageNamed: [_btnImgONArr objectAtIndex:i]] forState:UIControlStateNormal];
	}
	
}


-(void)highlightTrump {
	
	NSLog(@"highlightTrump()\n");
	if (_trumpSuit != NULL) {
		
		switch (_trumpSuitID) {
			case 1:
				NSLog(@"Spade selected");				
				[self enableDisableTrump:_spadeSelected];
				break;
			case 2:
				NSLog(@"Club selected");
				[self enableDisableTrump:_clubSelected];
				break;
			case 3:
				NSLog(@"Diamond selected");
				[self enableDisableTrump:_diamondSelected];
				break;
			case 4:
				NSLog(@"Heart selected");
				[self enableDisableTrump:_heartSelected];
				break;
			case 5:
				NSLog(@"No Trump selected");
				[self enableDisableTrump:_noTrumpSelected];
				break;
			//default:
			//	break;
		}
	
		boolTrumpSelected = YES;
	}
	[self checkViewCompletion];
}

-(void)checkViewCompletion {
	
	// TO-DO: update teh comment
	//Check on the completion items
	NSLog(@"\n\n");
	NSLog(@"checkViewCompletion()");
	
	NSLog(@"boolBidSet: %@ %@",boolBidSet ? @"Yes -->" : @"No",_txtHandBid.text);
	NSLog(@"boolTrumpSelected: %@ %@",boolTrumpSelected ? @"Yes -->" : @"No", _trumpSuit);
	NSLog(@"boolBidTeamSet: %@ %@",boolBidTeamSet ? @"Yes -->" : @"No", _biddingTeam);
	
	
	if (boolBidSet == YES && boolTrumpSelected == YES && boolBidTeamSet == YES) {
		NSLog(@"View Setup Complete... enable the close button");
		[self enableCloseButton];
	}
	
	NSLog(@"\n\n");
}

- (IBAction)bidTeamA:(id)sender {
	[self setupBiddingTeam:@"teamA"];
}

- (IBAction)bidTeamB:(id)sender {
	[self setupBiddingTeam:@"teamB"];
}

-(void)setupBiddingTeam:(NSString *) biddingTeam {
	
	NSLog(@"setupBiddingTeam()");

	// Set to class var so the GameController can grab it.
	_biddingTeam = biddingTeam;

	if ([biddingTeam isEqualToString:@"teamA"]) {
		[_btnBidTeamA setImage: [UIImage imageNamed:@"btn_radio_greenON"] forState:UIControlStateNormal];
		[_btnBidTeamB setImage:[UIImage imageNamed:@"btn_radioOFF"] forState:UIControlStateNormal];
	} else if ([biddingTeam isEqualToString:@"teamB"]) {
		[_btnBidTeamA setImage:[UIImage imageNamed:@"btn_radioOFF"] forState:UIControlStateNormal];
		[_btnBidTeamB setImage:[UIImage imageNamed:@"btn_radio_greenON"] forState:UIControlStateNormal];
	}

	boolBidTeamSet = YES;
	[self checkViewCompletion];
}

-(void)disableCloseButton {
	_btnClose.enabled = false;
	[_btnClose setImage: [UIImage imageNamed:@"btn_closeOFF"] forState:UIControlStateNormal];
}
-(void)enableCloseButton {
	_btnClose.enabled = true;
	[_btnClose setImage: [UIImage imageNamed:@"btn_closeON"] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated {
	
	NSLog(@"\n\n");
	
	NSLog(@"\n\nTrumpViewController: viewWillAppear()");

	NSLog(@"trumpSuit: %@",_trumpSuit);
	NSLog(@"trumpSuitID: %i",_trumpSuitID);
	
	NSLog(@"_teamA: %@",_teamA);
	NSLog(@"_teamB: %@",_teamB);

	if (_biddingTeam != NULL) {
		
		boolBidTeamSet = YES;
		[self setupBiddingTeam:_biddingTeam];

	}
	
	[self highlightTrump];
	
	_txtHandBid.text = _handBid;
	
	if ([_handBid length] > 0 && [_trumpSuit length] > 0) {
		NSLog(@"GTG... enable the close button");
		[self enableCloseButton];
	}
	
	_lblTeamA.text = _teamA;
	_lblTeamB.text = _teamB;

}


-(void)customizeInterface {
	
	self.view.backgroundColor = [UIColor clearColor];
	
	AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	
	NSLog(@"boolIsIphone5: %@",appDelegate.boolIsIphone5 ? @"Yes" : @"No");

	
	if (appDelegate.boolIsIphone5 == YES) {
		_imgPanelBackground.image = [UIImage imageNamed:@"panel_trump-568h"];
	}
	
}


- (void)viewDidLoad {
	
	
	NSLog(@"\n\nTrumpViewController: viewDidLoad()");
	
	
	[self customizeInterface];
    
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	boolTrumpSelected = NO;
	boolBidSet = NO;
	boolBidTeamSet = NO;
	boolNewBid = NO;
	
	
	[_trumpScroller setScrollEnabled:YES];
	[_trumpScroller setContentSize:CGSizeMake(320, 501)];
	
	_btnArr = [NSArray arrayWithObjects: _clubSelected, _spadeSelected, _diamondSelected, _heartSelected, _noTrumpSelected, nil];
	_btnImgOFFArr = [NSArray arrayWithObjects:@"trump_clubOFF",@"trump_spadeOFF",@"trump_diamondOFF",@"trump_heartOFF",@"trump_jokerOFF", nil];
	_btnImgONArr = [NSArray arrayWithObjects:@"trump_clubON",@"trump_spadeON",@"trump_diamondON",@"trump_heartON",@"trump_jokerON", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Keyboard controls
/* Setup the keyboard controls */
-(void)setupKeyboardControls {
	
    // Initialize the keyboard controls
    self.keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    self.keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    self.keyboardControls.textFields = [NSArray arrayWithObjects:
										self.txtHandBid,nil ];
	//										self.password, nil];
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    self.keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    self.keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    self.keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    self.keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    self.keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
	
	
	[self.keyboardControls reloadTextFields];
}



/* Scroll the view to the active text field */
-(void)scrollViewToTextField:(id)textField {
    UITableViewCell *cell = nil;
    if ([textField isKindOfClass:[UITextField class]])
        cell = (UITableViewCell *) ((UITextField *) textField).superview.superview;
    else if ([textField isKindOfClass:[UITextView class]])
        cell = (UITableViewCell *) ((UITextView *) textField).superview.superview;
	//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


/*
 * The "Done" button was pressed
 * We want to close the keyboard
 */
-(void)keyboardControlsDonePressed:(BSKeyboardControls *)controls {
    [controls.activeTextField resignFirstResponder];
}
/* Either "Previous" or "Next" was pressed
 * Here we usually want to scroll the view to the active text field
 * If we want to know which of the two was pressed, we can use the "direction" which will have one of the following values:
 * KeyboardControlsDirectionPrevious "Previous" was pressed
 * KeyboardControlsDirectionNext "Next" was pressed
 */
-(void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField {
    [textField becomeFirstResponder];
    [self scrollViewToTextField:textField];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
	
	if ([self.keyboardControls.textFields containsObject:textField])
		self.keyboardControls.activeTextField = textField;
	[self scrollViewToTextField:textField];
	
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	
	if (heightFraction < 0.0) {
		heightFraction = 0.0;
	} else if (heightFraction > 1.0) {
		heightFraction = 1.0;
	}
	
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
		animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
		
	} else {
		animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
	
	
}

-(void)textFieldDidEndEditing:(UITextField *)textField  {
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
 	[UIView commitAnimations];
}

-(IBAction)dismissKeyboard:(id)sender {
	[self.txtHandBid resignFirstResponder];
	
	if ([_txtHandBid.text length] > 0) {
		
		if ([_txtHandBid.text intValue] >= 11) {
			
			NSLog(@"Alert: User has entered a bid too high...");
			[self alertBidTooHigh:nil];
		} else {
			boolBidSet = YES;
		}
		
	}
	[self checkViewCompletion];
}


-(void)alertBidTooHigh:(id)sender {
	
	NSString *message = @"Your bid is set to high.";
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
													message:message
												   delegate:self
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert setTag:1];
	[alert show];
	message = nil;
	
	_txtHandBid.text = @"- -";
	
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	
	// "Star Over" alert actions
	switch (alertView.tag) {
		case 1:
		case 2:
		case 3:
			if (buttonIndex == 0) {
				NSLog(@"OK button clicked");
//				[self startOver];
			} else if (buttonIndex ==1) {
				NSLog(@"CANCEL button clicked");
			}
			break;
			
		default:
			break;
	}
}


@end
