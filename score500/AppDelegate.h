//
//  AppDelegate.h
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>


// Utilities
#import "Colors.h"

@import Firebase;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign) BOOL boolIsIphone5;

@property (assign) BOOL isFreeVersion;

@property (assign) BOOL homeScreenRestart;


@property (assign) CGFloat deviceHeight;
@property (assign) CGFloat screenHeight;
@property (assign) CGFloat screenWidth;
@property (assign) CGFloat tutorialSizeModifier;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

// save some typing getting this delegate
+(AppDelegate*)app;

@end
