//
//  ScoreCardViewController.m
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "ScoreCardViewController.h"

@interface ScoreCardViewController ()

@end

@implementation ScoreCardViewController



- (void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	// Show the Ad Banners if this is the Free/Lite version
	if (appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];
	
	
	CGFloat height = 1035;
	
	if (appDelegate.screenHeight == 480) {
		_cellHeight = 50;
		height = 980;
	} else {
		_cellHeight = 65;
		height = 1035;
	}
	
	
	CGFloat width = 288;
	
	[_scrollView setScrollEnabled:YES];
	[_scrollView setContentSize:CGSizeMake(width, height)];
	
	// Disable the inset that iOS 7 adds by default
	self.automaticallyAdjustsScrollViewInsets = NO;
	
	
	[self setupTrumpData];
	
	
	//	self.canDisplayBannerAds = YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc {
	_bannerView.delegate = nil;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {

	return _cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSLog(@"table Cell should go here....");

	static NSString *cellIdentifier = @"Cell";
	RulesCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	NSString *trumpSuit;
	switch (indexPath.row) {
		case 0:
			trumpSuit = [@"rule_" stringByAppendingString: [[_spade_info_arr objectAtIndex:0] lowercaseString]];
			cell.lblTricks7.text = [NSString stringWithFormat:@"%@", [_spade_info_arr objectAtIndex:1]];
			cell.lblTricks8.text = [NSString stringWithFormat:@"%@",  [_spade_info_arr objectAtIndex:2]];
			cell.lblTricks9.text = [NSString stringWithFormat:@"%@", [_spade_info_arr objectAtIndex:3]];
			cell.lblTricks10.text = [NSString stringWithFormat:@"%@",  [_spade_info_arr objectAtIndex:4]];
			break;
		case 1:
			trumpSuit = [@"rule_" stringByAppendingString: [[_club_info_arr objectAtIndex:0] lowercaseString]];
			cell.lblTricks7.text = [NSString stringWithFormat:@"%@", [_club_info_arr objectAtIndex:1]];
			cell.lblTricks8.text = [NSString stringWithFormat:@"%@",  [_club_info_arr objectAtIndex:2]];
			cell.lblTricks9.text = [NSString stringWithFormat:@"%@", [_club_info_arr objectAtIndex:3]];
			cell.lblTricks10.text = [NSString stringWithFormat:@"%@",  [_club_info_arr objectAtIndex:4]];
			break;

		case 2:
			trumpSuit = [@"rule_" stringByAppendingString: [[_diamond_info_arr objectAtIndex:0] lowercaseString]];
			cell.lblTricks7.text = [NSString stringWithFormat:@"%@", [_diamond_info_arr objectAtIndex:1]];
			cell.lblTricks8.text = [NSString stringWithFormat:@"%@",  [_diamond_info_arr objectAtIndex:2]];
			cell.lblTricks9.text = [NSString stringWithFormat:@"%@", [_diamond_info_arr objectAtIndex:3]];
			cell.lblTricks10.text = [NSString stringWithFormat:@"%@",  [_diamond_info_arr objectAtIndex:4]];
			break;

		case 3:
			trumpSuit = [@"rule_" stringByAppendingString: [[_heart_info_arr objectAtIndex:0] lowercaseString]];
			cell.lblTricks7.text = [NSString stringWithFormat:@"%@", [_heart_info_arr objectAtIndex:1]];
			cell.lblTricks8.text = [NSString stringWithFormat:@"%@",  [_heart_info_arr objectAtIndex:2]];
			cell.lblTricks9.text = [NSString stringWithFormat:@"%@", [_heart_info_arr objectAtIndex:3]];
			cell.lblTricks10.text = [NSString stringWithFormat:@"%@",  [_heart_info_arr objectAtIndex:4]];
			break;
			
		case 4:
			trumpSuit = @"rule_jokers";
			cell.lblTricks7.text = [NSString stringWithFormat:@"%@", [_noTrump_info_arr objectAtIndex:1]];
			cell.lblTricks8.text = [NSString stringWithFormat:@"%@",  [_noTrump_info_arr objectAtIndex:2]];
			cell.lblTricks9.text = [NSString stringWithFormat:@"%@", [_noTrump_info_arr objectAtIndex:3]];
			cell.lblTricks10.text = [NSString stringWithFormat:@"%@",  [_noTrump_info_arr objectAtIndex:4]];
			break;
		default:
			break;
	}
	
	cell.imgTrump.image = [UIImage imageNamed: trumpSuit];
    
    return cell;
}


-(NSMutableArray *)fetchTrumpData:(NSString *)trumpPredicate {
	
	
	NSError *error;
	NSMutableArray *dataArr;
	
    NSLog(@"Fetching data for the entity named -----> %@", @"Trump");
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"trumpSuit = %@ AND trickCount >= 7 AND trickCount <= 10",trumpPredicate];
	
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Trump" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	dataArr = [[NSMutableArray alloc]initWithArray:fetchedObjects];
	
	return dataArr;
}





-(void)customizeInterface {
	
	
}


-(void)setupTrumpData {
	
	/**
	 * Fetch data for card suits
	 */
	_spadeData = [self fetchTrumpData:@"Spades"];
	_clubData = [self fetchTrumpData:@"Clubs"];
	_diamondData = [self fetchTrumpData:@"Diamonds"];
	_heartData = [self fetchTrumpData:@"Hearts"];
	_noTrumpData = [self fetchTrumpData:@"No Trump"];
	
	
	/**
	 * Parse them out into edible arrays...
	 */
	_spade_info_arr = [NSMutableArray array];
	for (int i = 0; i < 4; i++) {
		_trump_info = [_spadeData objectAtIndex:i];
		[_spade_info_arr addObject: _trump_info.trumpValue];
	}
	[_spade_info_arr insertObject:_trump_info.trumpSuit atIndex:0];
	
	
	_club_info_arr = [NSMutableArray array];
	for (int i = 0; i < 4; i++) {
		_trump_info = [_clubData objectAtIndex:i];
		[_club_info_arr addObject: _trump_info.trumpValue];
	}
	[_club_info_arr insertObject:_trump_info.trumpSuit atIndex:0];
	
	
	_diamond_info_arr = [NSMutableArray array];
	for (int i = 0; i < 4; i++) {
		_trump_info = [_diamondData objectAtIndex:i];
		[_diamond_info_arr addObject: _trump_info.trumpValue];
	}
	[_diamond_info_arr insertObject:_trump_info.trumpSuit atIndex:0];
	
	_heart_info_arr = [NSMutableArray array];
	for (int i = 0; i < 4; i++) {
		_trump_info = [_heartData objectAtIndex:i];
		[_heart_info_arr addObject: _trump_info.trumpValue];
	}
	[_heart_info_arr insertObject:_trump_info.trumpSuit atIndex:0];
	
	_noTrump_info_arr = [NSMutableArray array];
	for (int i = 0; i < 4; i++) {
		_trump_info = [_noTrumpData objectAtIndex:i];
		[_noTrump_info_arr addObject: _trump_info.trumpValue];
	}
	[_noTrump_info_arr insertObject:_trump_info.trumpSuit atIndex:0];
	
	
	NSLog(@"spade Arr %@",_spade_info_arr);
	NSLog(@"club Arr %@",_club_info_arr);
	NSLog(@"diamond Arr %@",_diamond_info_arr);
	NSLog(@"heart Arr %@",_heart_info_arr);
	NSLog(@"no trump Arr %@",_noTrump_info_arr);
}



#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);

	self.bannerView.delegate = self;

	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		// Move the start/stop button up...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		float xPosSettings = _btn_settings.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPos = yPos - 50;
//		yPosSettings = yPosSettings - 10;
		
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the start/stop button back down...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		yPos = yPos + 50;
		
//		float xPosSettings = _btns_start_stop.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPosSettings = yPosSettings + 10;
		

		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}





@end
