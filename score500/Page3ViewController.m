//
//  Page3ViewController.m
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import "Page3ViewController.h"

@interface Page3ViewController ()

@end

@implementation Page3ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)page2ButtonTapped:(UIBarButtonItem *)sender
{
    [self.rootViewController goToPreviousContentViewController];
}

@end
