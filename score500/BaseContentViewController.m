//
//  BaseContentViewController.m
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import "BaseContentViewController.h"

@interface BaseContentViewController ()

@end

@implementation BaseContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
