//
//  InfoViewController.m
//  Score500
//
//  Created by Drew Bombard on 12/22/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import "InfoViewController.h"


@interface InfoViewController ()

@end


@implementation InfoViewController




-(void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	// Show the Ad Banners if this is the Free/Lite version
	if (appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];
	
	
	NSString *_version = @"v. ";
	_lblVersionNum.text = [_version stringByAppendingString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	NSString *yearString = [formatter stringFromDate:[NSDate date]];
	
	_lblCopyright.text = [[ @"© " stringByAppendingString:yearString] stringByAppendingString:[ @" " stringByAppendingString:@"default_method"]];
}


-(void)customizeInterface {
	
//	[[UINavigationBar appearance] setTintColor:[UIColor redColor]];
//	[[UINavigationBar appearance] setBarTintColor:[Colors get].darkYelllow];
}

-(void)dealloc {
	_bannerView.delegate = nil;
}


#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
// Move the start/stop button up...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		float xPosSettings = _btn_settings.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPos = yPos - 50;
//		yPosSettings = yPosSettings - 10;
		
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the start/stop button back down...
//		float xPos = _btns_start_stop.center.x;
//		float yPos = _btns_start_stop.center.y;
		
//		yPos = yPos + 50;
		
//		float xPosSettings = _btns_start_stop.center.x;
//		float yPosSettings = _btn_settings.center.y;
		
//		yPosSettings = yPosSettings + 10;
		
		
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
//							 _btns_start_stop.center = CGPointMake(xPos, yPos);
//							 _btn_settings.center = CGPointMake(xPosSettings, yPosSettings);
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}














-(IBAction)tellYourFriends:(id)sender {
	
	NSMutableArray *sharingItems = [NSMutableArray new];
	
	NSString *share_text = @"Hey, checkout default_method and the app Frolfer. It's pretty sweet.\n";
	NSString *share_url = @"http://www.defaultmethod.com";
	UIImage *share_img = [UIImage imageNamed:@"dm_logo"];
	
	[sharingItems addObject: share_text];
	[sharingItems addObject: share_url];
	[sharingItems addObject: share_img];
	
	
	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
	
	
	[self presentViewController:activityController animated:YES completion:nil];
}

-(IBAction)openMail:(id)sender {
	
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
		
		mailer.mailComposeDelegate = self;
		
		[mailer setSubject:@"Feedback"];
		
		NSArray *toRecipients = [NSArray arrayWithObjects:@"support@defaultmethod.com", nil];
		[mailer setToRecipients:toRecipients];
		
		//		UIImage *myImage = [UIImage imageNamed:@"dm_logo.png"];
		//		NSData *imageData = UIImagePNGRepresentation(myImage);
		//		[mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
		
		//NSString *emailBody = @"Have a question or comment?\n\n";
		NSString *emailBody = @"";
		[mailer setMessageBody:emailBody isHTML:NO];
		
		[self presentViewController:mailer animated:YES completion:nil];
		
	} else {
		
		UIAlertController * alert = [UIAlertController
									 alertControllerWithTitle:@"Error"
									 message:@"Your device doesn't support the composer sheet"
									 preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction* yesButton = [UIAlertAction
									actionWithTitle:@"OK"
									style:UIAlertActionStyleDefault
									handler:^(UIAlertAction * action) {
										NSLog(@"OK button clicked");
									}];
		
		[alert addAction:yesButton];
		
		[self presentViewController:alert animated:YES completion:nil];
	}
 
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the drafts folder.");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
			break;
		default:
			NSLog(@"Mail not sent.");
			break;
	}
 
	// Remove the mail view
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dmWebsite:(id)sender {
	[self openExternalLink:@"http://www.defaultmethod.com"];
}

- (IBAction)appStoreLink:(id)sender {
	[self openExternalLink:@"https://itunes.apple.com/us/app/score500/id996313263?ls=1&mt=8"];
}

-(void)openExternalLink:(NSString *)externalLink {
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: externalLink]];
}



@end
