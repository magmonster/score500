//
//  GamePlayViewController.h
//  Score500
//
//  Created by Drew Bombard on 4/12/16.
//  Copyright © 2016 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


// Utilities
#import "Colors.h"
#import "Constants.h"

// Banners
@class GADBannerView;
@import GoogleMobileAds;


@interface GamePlayViewController : UITableViewController <GADBannerViewDelegate>


-(void)bannerConfig;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutlet UITableView *gamePlayTableView;

@property (assign, nonatomic) BOOL bannerIsVisible;

@property (strong, nonatomic) GADBannerView *bannerView;





@end
