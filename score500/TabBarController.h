//
//  TabBarController.h
//  Score500
//
//  Created by Drew Bombard on 1/25/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "GameViewController.h"
#import "SetupViewController.h"
#import "InfoViewController.h"


@interface TabBarController : UITabBarController <UITabBarControllerDelegate>


@end
