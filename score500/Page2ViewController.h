//
//  Page2ViewController.h
//  Score500
//
//  Created by Drew Bombard on 8/6/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseContentViewController.h"

@interface Page2ViewController : BaseContentViewController

#pragma mark - Actions
- (IBAction)page1ButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)page3ButtonTapped:(UIBarButtonItem *)sender;

@end
