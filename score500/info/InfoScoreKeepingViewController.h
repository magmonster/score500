//
//  InfoScoreKeepingViewController.h
//  Score500
//
//  Created by Drew Bombard on 2/4/16.
//  Copyright © 2016 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


// Utilities
#import "Colors.h"
#import "Constants.h"

// Banners
@class GADBannerView;
@import GoogleMobileAds;


@interface InfoScoreKeepingViewController : UIViewController <GADBannerViewDelegate, UIWebViewDelegate>

-(void)bannerConfig;

@property (strong, nonatomic) AppDelegate *appDelegate;


@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;


@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *htmlFile;
@property (strong, nonatomic) NSString *htmlString;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end



