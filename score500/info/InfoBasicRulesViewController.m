//
//  InfoBasicRulesViewController.m
//  Score500
//
//  Created by Drew Bombard on 12/21/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import "InfoBasicRulesViewController.h"

@interface InfoBasicRulesViewController ()

@end

@implementation InfoBasicRulesViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	// Grab the local DB and init the App Delegate
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	
	
	// Show the Ad Banners if this is the Free/Lite version
	if (_appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
	
	[self customizeInterface];
	[self.navigationController setToolbarHidden:YES];

	
	_url =[[NSBundle mainBundle] bundleURL];
	_htmlFile = [[NSBundle mainBundle] pathForResource:@"basic_rules" ofType:@"html"];
	_htmlString = [NSString stringWithContentsOfFile:_htmlFile encoding:NSUTF8StringEncoding error:nil];
	
	self.webview.delegate = self;
	[_webview loadHTMLString:_htmlString baseURL:_url];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
	if ( inType == UIWebViewNavigationTypeLinkClicked ) {
		[[UIApplication sharedApplication] openURL:[inRequest URL]];
		return NO;
	}
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)customizeInterface {

}


#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	// Replace this ad unit ID with your own ad unit ID.
	self.bannerView.adUnitID = [Constants get].sampleAdUnitID;
	
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		

		CGRect shrinkWebView = _webview.frame;
		shrinkWebView.size.height = _webview.frame.size.height - _bannerView.frame.size.height;
		[_webview setFrame:shrinkWebView];

		
		
		
//		// Move the view down
//		[UIView animateWithDuration:0.2
//						 animations:^{
//							 
//						 }];
//
//		[UIView commitAnimations];
	}
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		
		_bannerIsVisible = NO;
		
	
		// Move the view down
		[UIView animateWithDuration:0.2
						 animations:^{
							CGRect shrinkWebView = _webview.frame;
							shrinkWebView.size.height = _webview.frame.size.height + _bannerView.frame.size.height;
							[_webview setFrame:shrinkWebView];
						 }];
	
		[UIView commitAnimations];
	}	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}

@end
