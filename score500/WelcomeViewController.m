
//
//  WelcomeViewController.m
//  Score500
//
//  Created by Drew Bombard on 5/23/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "WelcomeViewController.h"


@interface WelcomeViewController ()

@end

@implementation WelcomeViewController


-(void)customizeInterface {

	_btnLetsPlay.backgroundColor = [Colors get].darkBlue;
	_btnLetsPlay.tintColor = [UIColor whiteColor];
	
	_btnTutorial.backgroundColor = [Colors get].darkOrange;
	_btnTutorial.tintColor = [UIColor whiteColor];
}


- (void)viewDidLoad {
	
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	if (_managedObjectContext == nil) {
		
		AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
		NSManagedObjectContext *context = [appDelegate managedObjectContext];
		[context setUndoManager:nil];
		
		_managedObjectContext = context;
	}
	
	
	/*
	 * Update local DB store for past users.
	 */
	NSNumber *appLastUpdated = nil;
	NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
	numFormat.numberStyle = NSNumberFormatterDecimalStyle;
	NSNumber *currentVersion =  [numFormat numberFromString:
								 [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
	
	if (_configDataArray == nil) {
		_configDataArray = [FetchDataArray dataFromEntity:@"Config" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
		
		if ([_configDataArray count] > 0) {
			_config_info = [_configDataArray objectAtIndex:0];
		} else {
			_config_info = [NSEntityDescription
							insertNewObjectForEntityForName:@"Config"
							inManagedObjectContext:_managedObjectContext];
		}
		appLastUpdated = _config_info.lastUpdated;
	}
//	NSLog(@"_config_info.lastUpdated: %@",_config_info.lastUpdated);
//	NSLog(@"appLastUpdated: %@",appLastUpdated);
//	NSLog(@"currentVersion: %@",currentVersion);
//
//	
	

	/**
	 * Check to see if we've got local records.
	 * If not, run appFirstRun() and parse through the jSON
	 */
	int local_data_count = [CoreDataCheck checkEntity:@"Team"];
	NSLog(@"Records found: %i", local_data_count);
	if (local_data_count == 0) {
		[self appFirstRun];
	}
	
	
	
	
	appLastUpdated = _config_info.lastUpdated;
	
	if (local_data_count > 0) {

		/*
		 * Update the score of the original install with new values
		 * for the 6-count trump value(s).
		 */
		if (!appLastUpdated || [appLastUpdated  isEqual: @0]) {
		
			NSLog(@"\n\nUpdate required... run update XX\n\n");
			
			NSError *error;
			NSMutableArray *dataArr;
			
			NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
			NSEntityDescription *entity = [NSEntityDescription entityForName:@"Trump" inManagedObjectContext:_managedObjectContext];
			[fetchRequest setEntity:entity];
			fetchRequest.predicate = [NSPredicate predicateWithFormat:@"trickCount == 6"];
			
			NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
			dataArr = [[NSMutableArray alloc]initWithArray:fetchedObjects];
			
			Trump *spade_info;
			Trump *club_info;
			Trump *diamond_info;
			Trump *heart_info;
			Trump *trump_info;
			
			if ([dataArr count] >= 1) {
				spade_info = [dataArr objectAtIndex:0];
				club_info = [dataArr objectAtIndex:1];
				diamond_info = [dataArr objectAtIndex:2];
				heart_info = [dataArr objectAtIndex:3];
				trump_info = [dataArr objectAtIndex:4];
			}
			
			spade_info.trumpValue	=	[NSNumber numberWithInt:40];
			club_info.trumpValue	=	[NSNumber numberWithInt:80];
			diamond_info.trumpValue	=	[NSNumber numberWithInt:120];
			heart_info.trumpValue	=	[NSNumber numberWithInt:160];
			trump_info.trumpValue	=	[NSNumber numberWithInt:200];
			
			_config_info.lastUpdated = currentVersion;
			
			[_managedObjectContext save:nil];
			
			NSLog(@"_config_info.lastUpdated: %@",_config_info.lastUpdated);
		}
	
	
		/*
		 * Check if the last updated version is less than what the latest buid is.
		 * If current build is newer, check for and run any patches...
		 */
		if (appLastUpdated != nil
			&& appLastUpdated < currentVersion) {
			
			NSLog(@"lastUpdated: %@",appLastUpdated);
			NSLog(@"currentVersion: %@",currentVersion);
			
			NSLog(@"\n\nUpdate required... run update XX\n\n");
		}
	}
	
	
	
	NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
	
	NSLog(@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]);
	if ([version isEqualToString:@"2.2"]) {
		
		NSLog(@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]);
		
	} else {
		
	}
	
	

	
	[self customizeInterface];
	
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	NSString *yearString = [formatter stringFromDate:[NSDate date]];

	_lblCopyright.text = [[ @"© " stringByAppendingString:yearString] stringByAppendingString:[ @" " stringByAppendingString:@"Default Method, LLC"]];

	
	NSString *_version = @"v. ";
	_lblVersionNum.text = [_version stringByAppendingString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
	

	NSString *firstRunCheck = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRunMoreThanOnce"]  ? @"Yes" : @"No";
	NSUserDefaults *setdefaults = [NSUserDefaults standardUserDefaults];
	[setdefaults setObject:@"Yes" forKey:@"isRunMoreThanOnce"];
	[setdefaults synchronize];
	NSLog(@"Data saved");
	
	if ([firstRunCheck isEqual: @"No"]) {
		
		NSLog(@"This is the FIRST time the app has ever run...");
	} else {
		NSLog(@"Updates are done. START the app..");
	}
	
	[self showMainView];
	
}


-(void)showMainView {
	
	[self performSegueWithIdentifier:@"loginSegue" sender:nil];
}


#pragma mark - App Setup


-(void)appFirstRunTrump:(NSMutableArray *)jsonData {
	
	NSError *error = nil;
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	
	for (NSDictionary *trump in [jsonData valueForKey:@"trump"]) {
		
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		NSEntityDescription *entityTrump = [NSEntityDescription
											entityForName:@"Trump"
											inManagedObjectContext:_managedObjectContext];
		[request setEntity:entityTrump];
		NSArray *objects = [_managedObjectContext executeFetchRequest:request error:&error];
		
		if (objects == nil || error) {
			NSLog(@"There was an error!");
		} else {
			
			Trump *_trump = (Trump *)[NSEntityDescription insertNewObjectForEntityForName:@"Trump"
																   inManagedObjectContext:_managedObjectContext];
			
			NSLog(@"\n trump: %@",[trump objectForKey:@"trumpSuit"]);
			
			NSNumber *nmb_trickCount = [numberFormatter numberFromString: [trump objectForKey:@"trickCount"]];
			NSNumber *nmb_trumpValue = [numberFormatter numberFromString: [trump objectForKey:@"trumpValue"]];
			
			[_trump setTrumpSuit: [trump objectForKey:@"trumpSuit"]];
			[_trump setTrickCount: nmb_trickCount];
			[_trump setTrumpValue: nmb_trumpValue];
			
			
			NSLog(@"\n");
			
			[_managedObjectContext save:&error];
		}
	}
}

-(void)appFirstRunTeam:(NSMutableArray *)jsonData {
	
	NSError *error = nil;
	
	if (error) {
		NSLog(@"%@", [error localizedDescription]);

	} else {

		for (NSDictionary *team in [jsonData valueForKey:@"team"]) {

			NSFetchRequest *request = [[NSFetchRequest alloc] init];
			NSEntityDescription *entityTeam = [NSEntityDescription
												entityForName:@"Trump"
												inManagedObjectContext:_managedObjectContext];
			[request setEntity:entityTeam];
			NSArray *objects = [_managedObjectContext executeFetchRequest:request error:&error];
			
			if (objects == nil || error) {
				NSLog(@"There was an error!");
			} else {
				
				Team *_team = (Team *)[NSEntityDescription insertNewObjectForEntityForName:@"Team"
																	   inManagedObjectContext:_managedObjectContext];
				
				[_team setTeam: [team objectForKey:@"team"]];
				[_team setTeamName: [team objectForKey:@"teamName"]];
				[_managedObjectContext save:&error];
			}

		}
	}
}

-(void)appFirstRunSetup:(NSMutableArray *)jsonData {
	
	
		
	NSError *error = nil;
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

	for (NSDictionary *setup in [jsonData valueForKey:@"setup"]) {
		
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		NSEntityDescription *entitySetup = [NSEntityDescription
										   entityForName:@"Setup"
										   inManagedObjectContext:_managedObjectContext];
		[request setEntity:entitySetup];
		NSArray *objects = [_managedObjectContext executeFetchRequest:request error:&error];
		
		if (objects == nil || error) {
			NSLog(@"There was an error!");
		} else {
			
			Setup *_setup = (Setup *)[NSEntityDescription insertNewObjectForEntityForName:@"Setup"
																inManagedObjectContext:_managedObjectContext];
			
			NSNumber *nmb_numRounds = [numberFormatter numberFromString: [setup objectForKey:@"numRounds"]];

			[_setup setGameType: [setup objectForKey:@"gameType"]];
			[_setup setNumRounds: nmb_numRounds];
			
			[_managedObjectContext save:&error];
		}
	}
}


-(void)appFirstRun {
	
	NSError *error = nil;
	NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"data_preload" ofType:@"json"];
	NSMutableArray *jsonData = [NSJSONSerialization
								JSONObjectWithData:[NSData dataWithContentsOfFile:dataPath]
								options:kNilOptions
								error:&error];
	
	NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
	numFormat.numberStyle = NSNumberFormatterDecimalStyle;
	NSNumber *currentVersion =  [numFormat numberFromString:
								 [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
	
	
	NSLog(@"json %@",jsonData);
	NSLog(@"\n\n");	
	if (error) {
		NSLog(@"%@", [error localizedDescription]);
	} else {
		[self appFirstRunTrump:jsonData];
		[self appFirstRunTeam:jsonData];
		[self appFirstRunSetup:jsonData];
	}

	
	_config_info.lastUpdated = currentVersion;
	
	[_managedObjectContext save:nil];
}








- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"alreadyRun"]) {

        // select the Today tab
        UITabBarController* tbc = segue.destinationViewController;
        tbc.selectedIndex = 0;
        [(UINavigationController*) tbc.selectedViewController popToRootViewControllerAnimated:NO];
    }
	else if ([[segue identifier] isEqualToString:@"loginSegue"]) {
		NSLog(@"loginSegue");
	}
}

- (IBAction)loadSetup:(id)sender {
//	[self performSegueWithIdentifier:@"setupSegue" sender:self];
}
@end
