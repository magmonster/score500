//
//  LocalUpdate.h
//  Score500
//
//  Created by Drew Bombard on 5/20/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface LocalUpdate : NSObject

+(void)appUpdate: (NSNumber *)currentVersion lastUpdate:(NSNumber *)lastUpdate;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
