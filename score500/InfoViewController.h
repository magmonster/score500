//
//  InfoViewController.h
//  Score500
//
//  Created by Drew Bombard on 12/22/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"

// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "Constants.h"


// Banners
@class GADBannerView;
@import GoogleMobileAds;


@class InfoViewController;


@interface InfoViewController : UITableViewController <MFMailComposeViewControllerDelegate,GADBannerViewDelegate>


-(IBAction)openMail:(id)sender;
-(IBAction)dmWebsite:(id)sender;
-(IBAction)appStoreLink:(id)sender;
-(IBAction)tellYourFriends:(id)sender;


@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@property (strong, nonatomic) IBOutlet UILabel *lblVersionNum;
@property (strong, nonatomic) IBOutlet UILabel *lblCopyright;





@end
