//
//  GoalsViewController.h
//  Score500
//
//  Created by Drew Bombard on 5/24/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Utilities
#import "Colors.h"
#import "Constants.h"

// Banners
@class GADBannerView;
@import GoogleMobileAds;

@class GoalsViewController;



@interface GoalsViewController : UIViewController <GADBannerViewDelegate>


-(void)bannerConfig;


@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;


//@property (strong, nonatomic) ADBannerView *bannerView;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutlet UIButton *btnTutorial;

@end
