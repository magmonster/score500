//
//  GameViewController.h
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AppDelegate.h"

#import "ScoringViewController.h"
#import "ScoringCell.h"
#import "SetupViewController.h"


// Data
#import "Team.h"
#import "Hand.h"
#import "Setup.h"
#import "Bidding.h"



// Utilities
#import "Colors.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "CoreDataCheck.h"
#import "UIView+Screenshot.h"


// Variable Definition
#define kTeamComponent 0
#define kSuitComponent 1
#define kBidComponent 2


// TRASH
// ScoringViewControllerDelegate
@interface GameViewController : UIViewController <
									UIAlertViewDelegate
									,UITableViewDataSource
									,UITableViewDelegate
									,UIPickerViewDelegate
									,UIPickerViewDataSource
									,NSFetchedResultsControllerDelegate> {

				
	BOOL gameplayReady;		
	int rowCounter;
}

@property (strong, nonatomic) IBOutlet UIImageView *currentBiddingTeam;
@property (strong, nonatomic) NSString *bidTeam;
@property (strong, nonatomic) NSString *biddingTeamName;
@property (strong, nonatomic) IBOutlet UIView *viewTeamNames;
@property (strong, nonatomic) IBOutlet UIView *viewTeam1;
@property (strong, nonatomic) IBOutlet UIView *viewTeam2;

@property (assign) int bidTeamReady;

@property (assign, nonatomic) CGFloat screenHeight;
@property (assign, nonatomic) CGFloat screenWidth;
@property (strong, nonatomic) IBOutlet UIPickerView *biddingPicker;
@property (strong, nonatomic) IBOutlet UIView *pickerBackgroundBlur;


@property (strong, nonatomic) NSArray *pickerTeamArray;
@property (strong, nonatomic) NSArray *pickerSuitArray;
@property (strong, nonatomic) NSArray *pickerBidArray;



@property (strong, nonatomic) NSArray *teamData;

@property (strong, nonatomic) Setup *setup_info;
@property (strong, nonatomic) NSArray *setupData;

@property (strong, nonatomic) Bidding *bidding_info;
@property (strong, nonatomic) NSArray *biddingData;

@property (strong, nonatomic) Hand *selected_hand;
@property (strong, nonatomic) NSMutableArray *handData;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@property (strong, nonatomic) IBOutlet UIButton *btnNoBid;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeBid;
@property (strong, nonatomic) IBOutlet UIButton *btnHideBid;


@property (strong, nonatomic) IBOutlet UIView *gradientMask;
@property (strong, nonatomic) IBOutlet UIView *viewBtnReset;

@property (strong, nonatomic) IBOutlet UIButton *btnAddScore;
-(IBAction)addScore:(id)sender;


// Incoming from the setup screen
@property (strong, nonatomic) NSString *gameType;
@property (retain) NSNumber *gameRounds;

@property (strong, nonatomic) IBOutlet UIView *noBidAlertBubble;



@property (strong,nonatomic) IBOutlet UITableView* tableView;



@property (strong, nonatomic) IBOutlet UILabel *lblTotalTeamScoreA;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalTeamScoreB;

@property (strong, nonatomic) IBOutlet UIImageView *imgGameTrump;

@property (strong, nonatomic) IBOutlet UIButton *btn_settings_team;
- (IBAction)showSetup:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblTeamA;
@property (strong, nonatomic) IBOutlet UILabel *lblTeamB;

@property (strong, nonatomic) NSString *teamNameA;
@property (strong, nonatomic) NSString *teamNameB;

-(void)alertNoBid;
-(IBAction)cancelAlert:(id)sender;



@property (strong, nonatomic) IBOutlet UIImageView *imgScorePanelGreen;
@property (strong, nonatomic) IBOutlet UIImageView *imgScorePanelBlue;


@property (strong, nonatomic) IBOutlet UIButton *btnScoring;


@property (strong, nonatomic) IBOutlet NSString *gameBid;
@property (strong, nonatomic) IBOutlet UILabel *lblNoBid;
@property (strong, nonatomic) IBOutlet UILabel *lblGameBid;
@property (strong, nonatomic) IBOutlet UIView *viewGameBid;

@property (strong, nonatomic) IBOutlet UIView *viewGameBidItems;





-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller;
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath: (NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath;
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type;







@property (strong, nonatomic) NSString *gameTrump;



@end
