//
//  Constants.h
//  score500
//
//  Created by Drew Bombard on 4/25/14.
//  Copyright (c) 2016 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject


+(Constants*)get;

@property (nonatomic,readonly) NSString *sampleAdUnitID;
@property (nonatomic,readonly) NSString *liveAdUnitID;

@end




