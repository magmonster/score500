//
//  SideMenuCell.h
//  Score500
//
//  Created by Drew Bombard on 4/5/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMenuItem;
@property (strong, nonatomic) IBOutlet UIImageView *imgMenuItem;

@end
