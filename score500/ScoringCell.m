//
//  ScoringCell.m
//  score500
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "ScoringCell.h"

@implementation ScoringCell

@synthesize lblScore = _lblScore;
@synthesize lblScoreA = _lblScoreA;
@synthesize lblScoreB = _lblScoreB;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
}

@end
