//
//  FetchDataArray.m
//  Frolfer
//
//  Created by Drew Bombard on 4/29/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "FetchDataArray.h"

@implementation FetchDataArray

+(NSMutableArray *)dataFromEntity:(NSString *)entityName predicateName:(NSString *)predicateName predicateValue:(NSString *)predicateValue predicateType:(NSString*)predicateType sortName:(NSString *)sortName sortASC:(BOOL *)sortASC {
	
	NSLog(@"\n\n\n");
	NSLog(@"FetchDataArray()");
	NSLog(@"predicateName: %@", predicateName);
	NSLog(@"predicateValue: %@", predicateValue);
	NSLog(@"predicateType: %@", predicateType);
	NSLog(@"\n\n");
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	
	NSError *error;
	NSMutableArray *dataArr;
	
    // 1 - Decide what Entity you want
    NSLog(@"Fetching data from the \"%@\" entity.\n", entityName);
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:entityName inManagedObjectContext:context];
	[fetchRequest setEntity:entity];
	
	// 3 - Filter it (optional)
	if (predicateName != NULL) {
		
		if (predicateType && [predicateType  isEqual: @"number"]) {
			fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@",predicateName, [NSNumber numberWithInt:[predicateValue intValue]]];
		} else {
			fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%@ == %@",predicateName,predicateValue];
		}
	}
	
//	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"trumpSuit == %@", @"Spades"];
	
	
	// 4 - Sort it (optional)
	if (sortName != NULL) {
		fetchRequest.sortDescriptors = [NSArray arrayWithObject:
										[NSSortDescriptor sortDescriptorWithKey:sortName
																	  ascending:YES
																	   selector:@selector(localizedCaseInsensitiveCompare:)]];
	}

	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	dataArr = [[NSMutableArray alloc]initWithArray:fetchedObjects];
//	NSLog(@"\n Returned data from \"%@\"\n %@",entityName, dataArr);
//	NSLog(@"\n\n\n");
	return dataArr;
}

@end
