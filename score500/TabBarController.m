//
//  TabBarController.m
//  Score500
//
//  Created by Drew Bombard on 1/25/16.
//  Copyright © 2016 default_method. All rights reserved.
//


#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController


- (void)viewDidLoad {
	[super viewDidLoad];

	// Set unselected states on tab bar items
	UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
	item0.image = [[UIImage imageNamed:@"menu_play"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

	UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
	item1.image = [[UIImage imageNamed:@"menu_setup"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

	UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];
	item2.image = [[UIImage imageNamed:@"menu_more"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (void)viewDidUnload {
	[super viewDidUnload];
//	// Release any retained subviews of the main view.

}

@end
