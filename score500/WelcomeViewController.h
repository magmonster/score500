//
//  WelcomeViewController.h
//  Score500
//
//  Created by Drew Bombard on 5/23/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Utility
#import "Colors.h"
#import "AppStats.h"
#import "LocalUpdate.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"

// TRASH
//#import "WBTabBarController.h"

// Data
#import "Config.h"
#import "Setup.h"
#import "Trump.h"
#import "Team.h"


@interface WelcomeViewController : UIViewController {
	
	NSTimer *activityTimer;
	BOOL gameSetupNeeded;
}


@property (strong, nonatomic) IBOutlet UIButton *btnTutorial;
@property (strong, nonatomic) IBOutlet UIButton *btnLetsPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnSetup;

- (IBAction)loadSetup:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblVersionNum;
@property (strong, nonatomic) IBOutlet UILabel *lblCopyright;

@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;



//@property (strong, nonatomic) Trump *trump;
@property (strong, nonatomic) NSArray *trumpData;


@property (strong, nonatomic) Config *config_info;
@property (strong, nonatomic) NSMutableArray *configDataArray;
@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;



@end
