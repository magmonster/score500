//
//  CoreDataCheck.m
//  Score500
//
//  Created by Drew Bombard on 4/24/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import "CoreDataCheck.h"

@implementation CoreDataCheck

+(int)checkEntity:(NSString *)entityName {
	
	NSLog(@"check_local_data(%@)",entityName);
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	
	// Check the Employees entity for local data...
	NSFetchRequest *local_request = [[NSFetchRequest alloc] init];
	[local_request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
	[local_request setIncludesSubentities:NO];
	
	NSError *err;
	NSUInteger count = [context countForFetchRequest:local_request error:&err];
	if(count == NSNotFound) {
		//Handle error
	}
	
	if (count > 0) {
		NSLog(@"yay......");
	} else {
		NSLog(@"NO records...");
	}
	
	return (int)count;
}

@end
