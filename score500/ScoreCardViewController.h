//
//  ScoreCardViewController.h
//  score500
//
//  Created by Drew Bombard on 2/12/13.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Data
#import "Trump.h"
#import "RulesCell.h"

// Utilities
#import "Colors.h"
#import "Constants.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "BSKeyboardControls.h"


// Banners
@class GADBannerView;
@import GoogleMobileAds;


@class ScoreCardViewController;



@interface ScoreCardViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,GADBannerViewDelegate>




-(void)bannerConfig;


@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;


@property (strong, nonatomic) IBOutlet UITableView* tableView;

@property (assign, nonatomic) CGFloat screenHeight;
@property (assign, nonatomic) CGFloat cellHeight;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) Trump *trump_info;

@property (strong, nonatomic) NSMutableArray *spadeData;
@property (strong, nonatomic) NSMutableArray *spade_info_arr;
@property (strong, nonatomic) NSMutableArray *clubData;
@property (strong, nonatomic) NSMutableArray *club_info_arr;
@property (strong, nonatomic) NSMutableArray *diamondData;
@property (strong, nonatomic) NSMutableArray *diamond_info_arr;
@property (strong, nonatomic) NSMutableArray *heartData;
@property (strong, nonatomic) NSMutableArray *heart_info_arr;
@property (strong, nonatomic) NSMutableArray *noTrumpData;
@property (strong, nonatomic) NSMutableArray *noTrump_info_arr;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
