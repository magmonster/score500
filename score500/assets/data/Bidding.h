//
//  Bidding.h
//  Score500
//
//  Created by Drew Bombard on 6/3/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Bidding : NSManagedObject

@property (nonatomic, retain) NSNumber * bidNum;
@property (nonatomic, retain) NSString * bidSuit;
@property (nonatomic, retain) NSString * bidTeam;
@property (nonatomic, retain) NSString * bidTeamName;
@property (nonatomic, retain) NSNumber * bidTeamIndex;
@property (nonatomic, retain) NSNumber * bidNumIndex;
@property (nonatomic, retain) NSNumber * bidSuitIndex;

@end
