//
//  Setup.h
//  Score500
//
//  Created by Drew Bombard on 5/14/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Setup : NSManagedObject

@property (nonatomic, retain) NSString * gameType;
@property (nonatomic, retain) NSNumber * numRounds;

@end
