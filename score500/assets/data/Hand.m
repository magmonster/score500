//
//  Hand.m
//  Score500
//
//  Created by Drew Bombard on 5/22/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import "Hand.h"


@implementation Hand

@dynamic bidCount;
@dynamic biddingTeam;
@dynamic bidSuit;
@dynamic bidWinner;
@dynamic scoreTeamA;
@dynamic scoreTeamB;
@dynamic sortOrder;
@dynamic tricksTeamA;
@dynamic tricksTeamB;

@end
