//
//  Config.h
//  
//
//  Created by Drew Bombard on 5/20/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Config : NSManagedObject

@property (nonatomic, retain) NSNumber * lastUpdated;

@end
