//
//  Trump.h
//  Score500
//
//  Created by Drew Bombard on 5/14/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Trump : NSManagedObject

@property (nonatomic, retain) NSNumber * trickCount;
@property (nonatomic, retain) NSString * trumpSuit;
@property (nonatomic, retain) NSNumber * trumpValue;

@end
