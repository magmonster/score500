//
//  Team.h
//  Score500
//
//  Created by Drew Bombard on 5/30/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Team : NSManagedObject

@property (nonatomic, retain) NSString * teamName;
@property (nonatomic, retain) NSString * team;

@end
