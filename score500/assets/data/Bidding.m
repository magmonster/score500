//
//  Bidding.m
//  Score500
//
//  Created by Drew Bombard on 6/3/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import "Bidding.h"


@implementation Bidding

@dynamic bidNum;
@dynamic bidSuit;
@dynamic bidTeam;
@dynamic bidTeamName;
@dynamic bidTeamIndex;
@dynamic bidNumIndex;
@dynamic bidSuitIndex;

@end
