//
//  Trump.m
//  Score500
//
//  Created by Drew Bombard on 5/14/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import "Trump.h"


@implementation Trump

@dynamic trickCount;
@dynamic trumpSuit;
@dynamic trumpValue;

@end
