//
//  Hand.h
//  Score500
//
//  Created by Drew Bombard on 5/22/14.
//  Copyright (c) 2014 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Hand : NSManagedObject

@property (nonatomic, retain) NSNumber * bidCount;
@property (nonatomic, retain) NSString * biddingTeam;
@property (nonatomic, retain) NSString * bidSuit;
@property (nonatomic, retain) NSString * bidWinner;
@property (nonatomic, retain) NSNumber * scoreTeamA;
@property (nonatomic, retain) NSNumber * scoreTeamB;
@property (nonatomic, retain) NSDate * sortOrder;
@property (nonatomic, retain) NSNumber * tricksTeamA;
@property (nonatomic, retain) NSNumber * tricksTeamB;

@end
