//
//  BiddingXViewController.h
//  Score500
//
//  Created by Drew Bombard on 12/30/15.
//  Copyright © 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "Constants.h"

// Data
#import "Team.h"
#import "Hand.h"
#import "Setup.h"
#import "Bidding.h"

// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "CoreDataCheck.h"
#import "UIView+Screenshot.h"

// Banners
@class GADBannerView;
@import GoogleMobileAds;


@class BiddingTeamViewController;

@interface BiddingTeamViewController : UIViewController <GADBannerViewDelegate>


@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (assign, nonatomic) BOOL bannerIsVisible;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@property (strong, nonatomic) IBOutlet UIButton *btnSave;

-(IBAction)saveBid:(id)sender;

@property (strong, nonatomic) NSString *bidTeam;
@property (strong, nonatomic) NSString *biddingTeamName;
@property (strong, nonatomic) IBOutlet UIPickerView *biddingPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *teamPickerView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentTeams;


@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) NSArray *pickerTeamArray;
@property (strong, nonatomic) NSArray *pickerSuitArray;
@property (strong, nonatomic) NSArray *pickerBidArray;


@property (strong, nonatomic) NSArray *teamData;
@property (strong, nonatomic) NSString *teamNameA;
@property (strong, nonatomic) NSString *teamNameB;
@property (strong, nonatomic) NSString *selectedTeamName;

@property (strong, nonatomic) Setup *setup_info;
@property (strong, nonatomic) NSArray *setupData;

@property (strong, nonatomic) Bidding *bidding_info;
@property (strong, nonatomic) NSArray *biddingData;

@property (strong, nonatomic) Hand *selected_hand;
@property (strong, nonatomic) NSMutableArray *handData;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
