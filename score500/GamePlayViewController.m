//
//  GamePlayViewController.m
//  Score500
//
//  Created by Drew Bombard on 4/12/16.
//  Copyright © 2016 default_method. All rights reserved.
//

#import "GamePlayViewController.h"

@interface GamePlayViewController ()

@end

@implementation GamePlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	// Grab the local DB and init the App Delegate
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
}


-(void)viewWillAppear:(BOOL)animated {
	
	// Show the Ad Banners if this is the Free/Lite version
	if (_appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	
	UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
	[footerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ProductCellBackground.png"]]];
	self.tableView.tableFooterView = footerView;
	[self.tableView setSeparatorStyle:(UITableViewCellSeparatorStyleNone)];
	[self.tableView setContentInset:(UIEdgeInsetsMake(0, 0, -500, 0))];
	
	[_gamePlayTableView addSubview:footerView];
	
	return nil;
}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//
////    return 9;
//}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Google AdMob (Banners)
-(void)bannerConfig {
	
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	CGRect footerRect = CGRectMake(0, 0, 320, 50);
	UIView *wrapperView = [[UIView alloc] initWithFrame:footerRect];
	
	
	GADBannerView *adView =[[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
	[self.view addSubview:adView];
	
	adView.adUnitID = [Constants get].sampleAdUnitID;
	adView.rootViewController = self;
	adView.delegate=(id<GADBannerViewDelegate>)self;
	
	GADRequest *request = [GADRequest request];
	[adView loadRequest:request];
	
	
	[wrapperView addSubview:adView];
	self.gamePlayTableView.tableFooterView = wrapperView;
}


// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Main: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		

		// TRASH LAter
//		CGRect shrinkWebView = _webview.frame;
//		shrinkWebView.size.height = _webview.frame.size.height - _bannerView.frame.size.height;
//		[_webview setFrame:shrinkWebView];
		
		
		
		
		//		// Move the view down
		//		[UIView animateWithDuration:0.2
		//						 animations:^{
		//
		//						 }];
		//
		//		[UIView commitAnimations];
	}
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Main: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		
		_bannerIsVisible = NO;
		

		// TRASH later
		// Move the view down
//		[UIView animateWithDuration:0.2
//						 animations:^{
//							 CGRect shrinkWebView = _webview.frame;
//							 shrinkWebView.size.height = _webview.frame.size.height + _bannerView.frame.size.height;
//							 [_webview setFrame:shrinkWebView];
//						 }];
		
		[UIView commitAnimations];
	}
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}


@end
